import cherrypy

def current_module():
    current_path = cherrypy.request.object_path
    module_name = current_path.split('/')[1]
    if module_name not in ['index', 'send_error_repport']:
        return module_name
    return None
