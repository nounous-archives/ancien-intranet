import cherrypy
from AuthorisationsManager import verifDroits

class ModuleBase:
    def category(self):
        return "Personnel"
    def title(self):
       return "Titre"
    def icon(self):
       return "icon.png"

    _droits = []
    _club = False
    _adh = True
    def droits(self):
       return self._droits

    def accessible(self):
        if not 'estClub' in cherrypy.session.keys():
            return False
        if cherrypy.session['estClub'] == True:
            if self._club == False:
                return False
        else:
            if self._adh == False:
                return False
        return verifDroits(cherrypy.session['droits'], self.droits())

