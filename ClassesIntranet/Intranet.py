#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
# Intranet.py
#
#     Classe Intranet, clase de base de l'intranet
#
# Copyright (c) 2006 by www.crans.org
# #############################################################
import sys
sys.path.append('/usr/scripts/intranet/')
from intranet_utils import cp as _crans_cp
import cherrypy
import os
make_path = os.path.join
sys.path.append('/usr/scripts/')
import cranslib.utils.exceptions
from ClassesIntranet.AuthorisationsManager import setDroits
from intranet_utils.mail import quickSend
import imp

class Intranet:
# ######################################################## #
#                 GESTION DES MODULES                      #
# ######################################################## #
#
#
    _loaded_modules = {}

    def _make_static_path_for_module(self, module_name ):
        return make_path("/", module_name, "static")

    def loadModule(self, un_module):
        MODULES_DIR = cherrypy.config.get("crans.modules.dir")
        if not (un_module.startswith(".") or un_module.startswith("_")):
            # faire ici l'importation
            # importer le fichier main.py
            try:
                #module_path =  MODULES_DIR + un_module
                module_path = make_path(MODULES_DIR,un_module)
                f, filename, desc = imp.find_module('main', [module_path])
                mon_module = imp.load_module('main', f, filename, desc)
                module_root = mon_module.main()
                # on ajoute la classe a l'arborescence de cherrypy :
                setattr( self, un_module, module_root)
                try:
                    # on ajoute le module aux modules connus
                    cat = module_root.category()
                    if not self._loaded_modules.has_key(cat):
                        self._loaded_modules[cat] = {}
                    self._loaded_modules[cat][un_module] = module_root
                    # on ajoute les droits du module :
                    droits_module = module_root.droits()
                    setDroits("/%s" % un_module, droits_module)
                except:
                    if cherrypy.config.get("server.environment") == "development":
                        _crans_cp.log("Impossible d'obtenir les parametres du module %s" % un_module)
                        _crans_cp.log(cranslib.utils.exceptions.formatExc())
            except:
                _crans_cp.log("Impossible de charger le module %s" % un_module)
                _crans_cp.log(cranslib.utils.exceptions.formatExc())
            # ajouter le dossier static ou il faut
            staticPath = make_path(MODULES_DIR, un_module, "static")
            if os.path.isdir(staticPath):
               settings= { self._make_static_path_for_module(un_module):
                   { 'sessionAuthenticateFilter.on': False,
                     'sessionFilter.on': False,
                     'server.output_filters.templatesEngine.on' : False,
                     'staticFilter.on': True,
                     'staticFilter.dir': staticPath,
                   }
               }
               cherrypy.config.update(settings)
               if cherrypy.config.get("server.environment") == "development":
                   _crans_cp.log("New static : %s" % staticPath)
                   # fin de l'ajout du dossier static


    def __init__(self):
        ##
        #### import automatique des modules
        ##
        MODULES_DIR = cherrypy.config.get("crans.modules.dir", False)
        if MODULES_DIR:
            if os.path.isdir(MODULES_DIR):
                sys.path.append(MODULES_DIR)
                Liste_Modules = os.listdir(MODULES_DIR)
                for un_module in Liste_Modules:
                    if un_module != "CVS":
                        self.loadModule(un_module)
            else:
                _crans_cp.log("Dossier des modules invalide", 'LOADING', 2)
        else:
            _crans_cp.log("Pas de dossier de modules", 'LOADING', 2)


# ######################################################## #
#                    QUELQUES PAGES                        #
# ######################################################## #
#
#
    def index(self, ticket=None):
        items = {}
        for a_category in self._loaded_modules:
            items[a_category] = {}
            for a_module_name in self._loaded_modules[a_category]:
                module_object = self._loaded_modules[a_category][a_module_name]
                if module_object.accessible():
                   items[a_category][a_module_name] = {}
                   items[a_category][a_module_name]["name"] = module_object.title()
                   items[a_category][a_module_name]["icon"] = self._make_static_path_for_module(a_module_name) + "/" + module_object.icon()
                   items[a_category][a_module_name]["url"] = "/" + a_module_name + "/"
            # si la categorie est vide, on la vire
            if items[a_category] == {}:
                del items[a_category]

        return {
            'template':'accueil',
            'values':{"modules":items},
            'stylesheets':['css/accueil.css'],
            }
    index.exposed= True

    def info(self):
        return {
          'template':'info-diverses',
          'values':{},
          'stylesheets':['accueil.css'],
        }
    info.exposed = True

    def send_error_repport(self, **kw):

        # entetes du mail
        exp = "intranet"
        dest = cherrypy.config.get("mail.bugreport", "nounous@crans.org")
        subject = "Rapport de Bug"

        # corps du mail
        text = """
        """

        # on recupere tout de suite le traceback
        tb = cranslib.utils.exceptions.formatExc()
        text += "\n= Traceback =\n"
        text += tb
        try:
            errorString = tb.split("\n")[-2]
            subject = "[Error] %s" % errorString
        except: pass

        text +="\n= Autres informations =\n"
        autres_informations = "\n".join( [ "%s: %s" % (str(a), str(kw[a])) for a in kw] )
        text += autres_informations
        text += "\n"

        #On ajoute des variables de cherrypy
        text += "\n= Cherrypy vars =\n"
        try: text += "user: %s\n" % cherrypy.session['uid']
        except: text += "user: <n/a>\n"
        try: text += "url: %s\n" % cherrypy.request.browser_url
        except: text += "url: <n/a>\n"
        #try:
        #    text += "headers: \n %s\n" %  "\n".join( ["    %s: %s" % (str(a), str(cherrypy.request.headers[a])) for a in cherrypy.request.headers] )
        #except:
        #    pass
        try:
            text += "query_string: %s\n" % cherrypy.request.query_string
        except:
            pass
        try:
            text += "path: %s\n" % cherrypy.request.path
        except:
            pass

        #on signe, quand meme !
        text += "\n-- \nRoot.py pour l'intranet\n"

        quickSend(exp, dest, subject, text)
        return self.index()

    send_error_repport.exposed = True

    def testErreur(self):
        raise Exception, u"Fausse alerte ! (test du syst�me de gestion des erreurs)"

    testErreur.exposed = True

    def _cp_on_http_error(self, status, message):
        if cherrypy.config.configMap["global"]["server.environment"] == "development":
            cherrypy._cputil._cp_on_http_error(status, message)
            return
        if cherrypy.config.configMap["global"]["displayBacktrace"] == "True":
            cherrypy._cputil._cp_on_http_error(status, message)
            return
        if status==403:
            cherrypy.response.body = {
                'template':'error403',
                'values':{'status':status, 'message':message },
                'standalone':False,
                }
        elif status==404:
            cherrypy.response.body = {
                'template':'error',
                'values':{'status':status, 'message':message },
                'standalone':False,
                }
        elif status==500:
            self.send_error_repport(status = status, message = message )
            # les filtres ne sont pas appliques � la main...
            from TemplatesManager import TemplatesFilter
            TemplatesFilter().goWithThisDict({'template':'error', 'values':{'status':status, 'message':cranslib.utils.exceptions.formatExc() }})
        else:
            self.send_error_repport(status = status, message = message)
            cherrypy._cputil._cp_on_http_error(status, message)
