from cherrypy.filters.basefilter import BaseFilter
import cherrypy._cputil

##########################
# DomFilter
##########################
#
# transforme des objets python
# en chainses de caracteres qui peuvent
# etre parsees avec JSON/javascript
#
class DOMFilter(BaseFilter):
    def beforeFinalize(self):
        body = cherrypy.response.body
        if isinstance(body, dict):
            body = self.printAsDom(body)
        cherrypy.response.body = body

    def printAsDom(self, chose):
        if isinstance(chose, dict):
            stringList = []
            for a_key in chose.keys():
                stringList.append('%s:%s' % (self.printAsDom(a_key), self.printAsDom(chose[a_key])))
            return "{%s}" % ','.join(stringList)

        if isinstance(chose, list):
            stringList = []
            for an_item in chose:
                stringList.append('%s' % (self.printAsDom(an_item)))
            return "[%s]" % ','.join(stringList)

        if isinstance(chose, str):
            return '"%s"' % chose

        if isinstance(chose, unicode):
            return '"%s"' % chose.encode('utf8')

        return str(chose)
