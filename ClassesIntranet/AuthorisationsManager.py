# -*- coding: iso-8859-15 -*-

from cherrypy.filters.basefilter import BaseFilter
import cherrypy._cputil
import cherrypy

##########################
# verification des droits
##########################
#
def verifDroits(mesDroits, lesDroitsQuilFaut):
    if not type(mesDroits) == list:
        raise ValueError, "mesDroits doit etre une liste"

    # Les droits "personnel" sont en fait des droits négatifs, au sens
    # où, lorsqu'on les a, on ne peut accéder aux pages qui ne les
    # requièrent pas. Lorsqu'on ne les a pas, en revanche, on peut quand
    # même accéder aux pages qui les requièrent. -- adg
    if "personnel" in mesDroits and not "personnel" in lesDroitsQuilFaut:
        return False
    elif "personnel" in lesDroitsQuilFaut:
        return True
    if (lesDroitsQuilFaut == "all") or (lesDroitsQuilFaut == []):
        return True
    if ("Nounou" in mesDroits):
        return True
    if type(lesDroitsQuilFaut) == str:
        return lesDroitsQuilFaut in mesDroits
    elif type(lesDroitsQuilFaut) == list:
        return True in [d in mesDroits for d in lesDroitsQuilFaut]
    return False

class AuthorisationsFilter(BaseFilter):

    def before_main(self):
        if not cherrypy.config.get('sessionAuthenticateFilter.on', False):
            return
        if not cherrypy.session.get("session_key"):
            return
        droits = cherrypy.config.get('crans.droits', 'all')
        if not verifDroits(cherrypy.session['droits'], droits):
            raise cherrypy.HTTPError(403, "Vous n'avez pas les droits n&eacute;cessaires pour acc&eacute;der &agrave; cette page.")

##########################
# mise en place des droits
##########################
#
def setDroits(chemin, lesDroitsQuilFaut):
    settings= {
       chemin:
          { 'crans.droits': lesDroitsQuilFaut}
    }
    cherrypy.config.update(settings)
