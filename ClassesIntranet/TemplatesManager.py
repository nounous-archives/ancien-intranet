# -*- coding: utf-8 -*-
from cherrypy.filters.basefilter import BaseFilter
import cherrypy
import os
from Cheetah.Template import Template
from ClassesIntranet import current_module

# ######################################################## #
#             Configuration de Cheetah                     #
# ######################################################## #

def serverSidePath(self, path):
    root_dir = cherrypy.config.configMap["global"]["rootDir"]
    if os.path.isfile(path):
        return path
    try:
        module_name = current_module()
        hyp_path = root_dir + '/modules/' + module_name + '/templates/' + path
        if os.path.isfile(hyp_path):
            return hyp_path
    except:
        pass
    return root_dir + '/templates/' + path

# on surcharge cette fonction dans la classe Template
Template.serverSidePath = serverSidePath


##########################
# templatesEngine
##########################
#
# Application des templates,
# avec plein de test chians
#
class TemplatesFilter(BaseFilter):
    def _getCorrectStaticMethod(self):
        try:
            module_name = current_module()
            if module_name != None:
               def static(truc):
                   return "/" + module_name + "/static/" + truc
               return static
        except:
            pass
        def static(truc):
            return "/static/" + truc
        return static


    def _getBodyTemplate(self, body):
        if isinstance(body, dict):
            if body.has_key('template'):
                if body['template'].endswith(".py"):
                    return body['template']
                elif body['template'].endswith(".tmpl"):
                    return body['template']
                else:
                    return body['template'] + ".tmpl"
        return False

    def _isStandaloneBody(self, body):
        if isinstance(body, dict):
            if body.has_key('standalone'):
                return body['standalone']
            if body.has_key('template'):
                return False
            else:
                return True
        return True


    def _getBodyNameSpace(self, body):
        if isinstance(body, dict):
            if body.has_key('values'):
                return body['values']
        return {}

    def _useMainTemplate(self, body):
        values = {'environment':cherrypy.config.configMap["global"]["server.environment"],
        'static':self._getCorrectStaticMethod(),
        }
        try:
            t = Template(file='main.tmpl', searchList= [body,{'login':cherrypy.session['uid']}, values])
        except:
            t = Template(file='main.tmpl', searchList=[body,{'login':''},values])

        return t.__str__()

    def goWithThisDict(self, aDict):
        body = aDict
        bodyTemplate = self._getBodyTemplate(body)
        if bodyTemplate:
            templatevalues = self._getBodyNameSpace(body)
            defaultvalues = {'static':self._getCorrectStaticMethod()}
            t = Template(file=bodyTemplate, searchList=[templatevalues, defaultvalues])
            body['page'] = t.__str__()

            if not self._isStandaloneBody(body):
                body = self._useMainTemplate(body)
            else:
                body = body["page"]
        cherrypy.response.body = body


    def beforeFinalize(self):

        body = cherrypy.response.body
        if isinstance(body, dict):
            self.goWithThisDict(body)
