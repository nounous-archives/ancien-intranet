#!/usr/bin/env python
import sys
import os
import time
sys.path.append('/usr/scripts/')
from cranslib.utils import files as tools
PDF_FOLDER = "/var/impression/fichiers/"

def cleanFolder(pathToFolder):
    print "cleaning %s" % pathToFolder
    if not os.path.isdir(pathToFolder):
        raise Exception, "%s n'est pas un dossier" % pathOfFolder
    fileList = os.listdir(pathToFolder)
    for aFile in fileList:
        aFilePath = os.path.join(pathToFolder, aFile)
        if os.path.isdir(aFilePath):
            cleanFolder(aFilePath)
            if tools.dirIsEmpty(aFilePath):
                os.rmdir(aFilePath)
                print "Folder %s removed" % aFilePath
        if os.path.isfile(aFilePath):
            if tools.fileIsOlderThan(aFilePath, days=1):
                os.remove(aFilePath)

now = time.localtime()
displaytime = time.strftime("%A %d %B %Y, %X",now)
print "................ %s ................" % displaytime
cleanFolder(PDF_FOLDER)
