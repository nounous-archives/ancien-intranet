import cherrypy
import xml.etree.ElementTree
import urllib, sys

def CASAuth(cas_server,service_url):
    if cherrypy.session.get('user'):
        #DEBUG: user is validated by existing session cookie
        cherrypy.session['validated_by'] = "session attribute"
        pass

    elif 'ticket' in cherrypy.request.params:
        #need to validate ticket
        ticket = cherrypy.request.params['ticket']

        #generate URL for ticket validation
        cas_validate = cas_server + "/serviceValidate?ticket=" + ticket + "&service=" + service_url
        f_xml_assertion = urllib.urlopen(cas_validate)
        if not f_xml_assertion:
            raise cherrypy.HTTPError(401, 'Unable to authenticate: trouble retrieving assertion from CAS to validate ticket.')

        #parse CAS XML assertion into a ElementTree
        assertion_tree = xml.etree.ElementTree.parse(f_xml_assertion)
        if not assertion_tree:
            raise cherrypy.HTTPError(401, 'Unable to authenticate: trouble parsing XML assertion.')

        #find <cas:user> in ElementTree
        attributes = {}
        for e in assertion_tree.findall('.//*'):
            print "DEBUG: Found tag '%s'" % e.tag
            if e.tag == '{http://www.yale.edu/tp/cas}authenticationFailure':
                 raise cherrypy.HTTPError(401, 'Unable to authenticate: authenticationFailure')
            if e.tag == "{http://www.yale.edu/tp/cas}user":
                user_name = e.text
            if e.tag == "{http://www.yale.edu/tp/cas}attributes":
                for attribute in e:
                    key=attribute.tag.split("}").pop()
                    value=attribute.text
                    if value.startswith('['):
                       value = value[1:-1].split(', ')
                    else:
                       value = [value]
                    attributes[key] = value
        if not user_name:
            #couldn't find <cas:user> in the tree
            raise cherrypy.HTTPError(401, 'Unable to validate ticket: could not locate cas:user element.')

        #add username to session
        cherrypy.session['user'] = user_name
        cherrypy.session['attributes'] = attributes


        #DEBUG: user is validated by initial ticket instance
        cherrypy.session['validated_by'] = "ticket"

        #if assertion_tree:
        #    assertion_tree.write('/tmp/xml')
        #option a
        #leaves ticket=..... param in browser's URL
        #del cherrypy.request.params['ticket']

        #option b
        #redirect to a clean service URL (without ticket=... param)
        #note: may cause usability issues if accessing a URL other than
        #initial Service URL with an expired session
        raise cherrypy.HTTPRedirect(service_url)

    else:
        #no existing session; no ticket to validate
        #redirect to CAS to retrieve new ticket
        raise cherrypy.HTTPRedirect(cas_server + "/login?service=" + service_url)
