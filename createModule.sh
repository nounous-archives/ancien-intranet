#!/bin/sh

if [ $# -lt 1 ]
then
   echo "usage: createModule <name>"
   exit
fi

name=$1

if [ -d modules/$name ]
then
   echo "This module exists"
   exit
fi

mkdir modules/$name/

echo """
from ClassesIntranet.ModuleBase import ModuleBase

class main(ModuleBase):
    _droits = []
    def category(self):
        return \"Cat.\"
    def title(self):
        return \"Titre\"

    def index(self):
        return {'template': 'defaut',
                'values':{},
		}
    index.exposed = True 
""" > modules/$name/main.py


mkdir modules/$name/templates

echo """
<div id="globalDiv">
<h1>Hello World!</h1>
</div>
""" > modules/$name/templates/defaut.tmpl