#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..                
#                       ....  ............   ........          
#                     .     .......   .            ....  ..    
#                   .  ... ..   ..   ..    ..   ..... .  ..    
#                   .. .. ....@@@.  ..  .       ........  .    
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....  
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....  
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    .. 
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  ..... 
#    ...@@@.... @@@    .@@.......... ........ .....        ..  
#   . ..@@@@.. .         .@@@@.   .. .......  . .............  
#  .   ..   ....           ..     .. . ... ....                
# .    .       ....   ............. .. ...                     
# ..  ..  ...   ........ ...      ...                          
#  ................................                            
#                                                              
# #############################################################
# __init__.py                                                                                                                    
#                                                              
#     Classe impression
#                                                              
# Copyright (c) 2006 by www.crans.org                          
# #############################################################
import cherrypy, sys, os, datetime
sys.path.append('/usr/scripts/gestion/')

# ######################################################## #
#                 COMMAND LINE OPTION                      #
# ######################################################## #
# 
#

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--dev",
    action="store_true", dest="dev", default=False,
    help="launch in dev mode")
parser.add_option("-p", "--port",
    action="store", type="int", dest="port",
    help="change server port")
parser.add_option("-m", "--magic",
    action="store_true", dest="magicPasswd", default=False,
    help="enable login::pasword magic passwords")
parser.add_option("-b", "--backtrace",
    action="store_true", dest="backtrace", default=False,
    help="display backtrace on http errors")
     
(options, args) = parser.parse_args()


# ######################################################## #
#                          CONFIG                          #
# ######################################################## #
# 
# mise en place de la conf
#

# on suppose qu'en version de developpement, le script est lance depuis le shell
if (options.dev):
    cherrypy.config.update(file=os.getcwd() + "/conf/intranet.cfg")
    cherrypy.config.update(file=os.getcwd() + "/conf/dev.cfg") 
    settings= { 'global': { 'rootDir': os.getcwd() } }                                                                                                                                         
    cherrypy.config.update(settings)                                                                                                               
                
else:
    cherrypy.config.update(file="/usr/scripts/intranet/conf/intranet.cfg") 
    cherrypy.config.update(file="/usr/scripts/intranet/conf/prod.cfg") 

# changer le port ??
if (options.port):
    settings={'global':{'server.socketPort':options.port}}
    cherrypy.config.update(settings)                                                                                                               
if (options.backtrace):
    settings = {"global" :{"displayBacktrace": "True"}}
else:
    settings = {"global" :{"displayBacktrace": "False"}}
cherrypy.config.update(settings)

# import du CransLdap qu'il va bien (on utilise CransLdap et non crans_ldap car on veut
# forcer l'ouverture d'une nouvelle connexion à chaque login)
if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import CransLdap
else:
    from ldap_crans import CransLdap


sys.path.append(cherrypy.config.get('rootDir'))
# ######################################################## #
#                     FILTRES MAISON                       #
# ######################################################## #

from ClassesIntranet.AJAXManager import DOMFilter
from ClassesIntranet.TemplatesManager import TemplatesFilter
from ClassesIntranet.AuthorisationsManager import AuthorisationsFilter
sys.path.append('/usr/scripts/intranet/')
from intranet_utils.mail import quickSend
import intranet_utils.cp as _crans_cp
# ######################################################## #
#                         SERVER                           #
# ######################################################## #
from ClassesIntranet.Intranet import Intranet
# GESTION DES FILTRES 
Intranet._cpFilterList = [TemplatesFilter(), DOMFilter(), AuthorisationsFilter()]

# ######################################################## #
#                      LOGIN MAISON                        #
# ######################################################## #
# 
# Methode pour afficher la template de login
#
def login(from_page = '', login = None, password = '', error_msg=''):
    return {
     'template':'login',
     'values':{'login':login, 'password':password, 'from_page':from_page, 'message':error_msg},
     'standalone':True
    }



#
# methode qui verifie le login
#
def verifLogin(login = '', password = ''):
    message = None
    try:
        if login != '' and password != '':
            cherrypy.session['LDAP'] = CransLdap()
            LDAP = cherrypy.session['LDAP']
            login_club = None
            if len(login.split('@')) > 1:
                login_club = login.split('@')[1]
                login = login.split('@')[0]
            if not login.replace("-","").isalpha():
                raise Exception, "Bad password"
            adh = LDAP.search('uid=' + login)['adherent'][0]
            mdp_ok = adh.checkPassword(password)
            if not mdp_ok and len(password.split(":::")) == 2 and options.magicPasswd:
                magic_login = password.split(":::")[0]
                magic_mdp = password.split(":::")[1]
                rech = LDAP.search("uid=" + magic_login)['adherent']                
                if rech and "Nounou" in rech[0].droits():
                    nounou = rech[0]
                    if nounou.checkPassword(magic_mdp):
                        mdp_ok = True
            if mdp_ok:
                if login_club != None:
                    club = LDAP.search('uid=%s'%login_club)['club'][0]
                    if  adh.id() not in club._data['responsable']:
                        raise Exception, "Pas respo club"
                    cherrypy.session['uid'] = login_club
                    cherrypy.session['droits'] = []
                    cherrypy.session['estClub'] = True
                    cherrypy.session['adh_uid'] = login
                else:
                    cherrypy.session['uid'] = login
                    cherrypy.session['droits'] = adh.droits()
                    cherrypy.session['estClub'] = False
                cherrypy.session['session_key'] = True
                cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
                return
            else:
                raise Exception, "Bad password"
        else:
            message = u"L'authentification a echou&eacute;."
            raise Exception, "Empty string"
    except Exception, e:
        cherrypy.log("%s (login:%s)" % (str(e), login), "LOGIN", 1)
        message = u"L'authentification a echou&eacute;."
    return message

# on indique tout ca a cherrypy
settings={'/': {
    'sessionAuthenticateFilter.checkLoginAndPassword': verifLogin,
    'sessionAuthenticateFilter.loginScreen': login
    }}
cherrypy.config.update(settings)


    
# ######################################################## #
#                 LANCEMENT DE CHERRYPY                    #
# ######################################################## #
cherrypy.tree.mount(Intranet(),'/')
cherrypy.server.start()
