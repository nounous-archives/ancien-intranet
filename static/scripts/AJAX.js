/*****************************
 AJAX
 *****************************/
AJAX = {};

AJAX.call = function(page, callBack, loadingMessage) {
   var loadingMessage = (loadingMessage == null) ? true : loadingMessage;
   //console.debug("calling AJAX : " + page);
   if (loadingMessage) {
       Crans.loading.display(true);
       var oldCallBack = callBack;
       callBack = function(r) {
           Crans.loading.display(false);
	   oldCallBack( r );
       };
   }
   var e = loadJSONDoc(page);
   e.addCallback(callBack);
   e.addErrback(AJAX.errorHandler);
}

AJAX.errorHandler = function(d) {
   Crans.loading.display(false);
   appendChildNodes(document.body, 
		    DIV({"class":"crans_ajax_error"},
			DIV({}, 
			    "Erreur de communication, essayez de vous ", 
			    A({"href":"do_logout"},  "reconnecter"), ".")
		    ));
   logError("AJAX Error: " + d);
   //Impression.AJAX.modifPrix("Erreur...", false);
}
