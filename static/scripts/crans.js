/* ************************************************************
 * Concole
 ************************************************************
 * Si firebug est présent, utilisation de la console intégrée
 * à la place de celle de MockieKit
 */
try {
/*  if (console.firebug) {
    log("Using Firebug console");
    log = function(m) {console.log(m)};
    logWarning = function(m){console.warn(m)};
    logInfo = function(m){console.info(m)};
    logDebug = function(m){console.debug(m)};
    logError = function(m){console.error(m)};
    logFatal = function(m){console.error(m)};
  }
*/
MochiKit.Logging.logger.useNativeConsole = true
}
catch (Exception) {}



/* ************************************************************
 * Crans
 ************************************************************
 * Crans.messages 		: afficher des messages sur les pages
 * Crans.loading                : afficher l'indicateur de chargement
 * Crans.keys                   : gestion des touches du clavier
 */
Crans = {};

/*****************************
 Crans.Messages
 *****************************/
Crans.messages = {}
Crans.messages.initialized = false;

Crans.messages.init = function()
{
   if (!Crans.messages.initialized)
     {
	//updateNodeAttributes(document.body, {'onclick':'Crans.messages.setMessage();'});
	appendChildNodes(document.body, DIV({'id':'_crans_main_message_place_holder'}));
	Crans.messages.initialized = true;
	logDebug("Crans.message initialized");
     }
}


Crans.messages.setMessage = function(m, messageClass)
{
   if (!Crans.messages.initialized)
     Crans.messages.init();
   if (m == null) {
      var messageBox = '';
   } else {
      if (messageClass==null)
	messageClass='message';
      if (messageClass == "errorMessage")
        logWarning(m);
      else
        log(m);
      var textHolder = SPAN({'class':'messageText'},m);
      var messageBox = DIV({'class':messageClass},textHolder);
      var messagePlace =  document.getElementById("_crans_main_message_place_holder");
   }
   try {
        var messagePlace =  document.getElementById("_crans_main_message_place_holder");
	replaceChildNodes(messagePlace,messageBox);
	try {roundElement(messageBox);} catch (error) {}
     }
   catch (error) {
	logError("�lement _crans_main_message_place_holder introuvable")
	  return
     }
}

/*****************************
 Crans.loading
 *****************************/
Crans.loading = {}
Crans.loading.initialized = false;

Crans.loading.init = function(){
   try {
      if (!Crans.loading.initialized) {
	 appendChildNodes(document.body, DIV({'id':'_crans_main_message_chargement'}, "Chargement..."));
	 Crans.loading.initialized = true;
      }
   } catch (error) {
      logError(error.description);
   }
}

Crans.loading.display = function(bool) {
   if (!Crans.loading.initialized)
     Crans.loading.init();
   var loadingEl =  document.getElementById("_crans_main_message_chargement");
   if (loadingEl) {
      if (bool) {
	 appear(loadingEl);
      } else {
	 fade(loadingEl, {duration:0.1});
      }
   } else {
       logFatal("Crans.loading cannot fid _crans_main_message_chargement element"); }
   return false;
}

/*****************************
 Crans.keys
 *****************************/
Crans.keys = {}
Crans.keys.handled = false;


Crans.keys.handleF1 = function()
{
   createLoggingPane(true);
};

Crans.keys.keyMap =
{
   'KEY_F1': Crans.keys.handleF1
//    , 'KEY_ESCAPE':alert
};


connect(document, 'onkeydown',
	function(e)
	{
	   // We're storing a handled flag to work around a Safari bug:
	   if (true)//(!Crans.keys.handled)
	     {

		var key = e.key();
		var fn = Crans.keys.keyMap[key.string];
		if (fn)
		  {
		     fn();
		  }

		//replaceChildNodes('onkeydown_code', key.code);
		//replaceChildNodes('onkeydown_string', key.string);
		//KeyEvents.updateModifiers(e);
	     }

	   Crans.keys.handled = true;
	}
	);