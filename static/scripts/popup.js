Popup = {};
Popup.popupNode = null;
Popup.visible = false;

Popup.display = function() 
{
   if (this.popupNode == null) {
      logError("Popup not created, cannot be displayed");
      return false;
   }
   appendChildNodes("pageContent", this.popupNode);
   this.visible = true;
   //        logDebug("popup visible");        
}

Popup.create = function(options, title_popup, content) {
   var inPopup = DIV({"id":"__popupInDivId", "style":"background:white;margin:2px 5px;"}, content); 
   var outPopup = DIV({"id":"__popupOutDivId","style":"background:#AE0F3E;z-index:500;float:left;padding:0;min-width:300px;position:fixed;top:30%;left:30%;right:30%;"}, H1({"style":"font-size:1em;margin:0;text-align:center;color:white;"}, IMG({"src":"/static/images/WindowTitleLogo.png","alt":"icon", "style":"margin:0 5px;"}), title_popup), inPopup );
   roundElement(outPopup);
   logDebug("Popup \""+ title_popup +"\" created");
   this.popupNode = outPopup;
}

Popup.hide = function() {
   if (this.visible) {
      removeElement(this.popupNode);
      this.visible = false;
   }
   //        logDebug("popup not visible");        
}
Popup.closeLink = function(options, text_link) {
   options["href"] = "#";
   options["onclick"] = "Popup.hide()";
   return A(options, text_link);
}
