"""
__init__.py
 
    Fonction de base pour les mails.
     
Copyright (c) 2006 by www.crans.org
"""

SENDMAIL = "/usr/sbin/sendmail" # sendmail location

def quickSend(From=None, To=None, Subject=None, Text=None, disconnect_on_bounce=0):
    """envoie d'un mail"""
    OPTIONS = ' -t' #extract recipient from headers
    if disconnect_on_bounce:
        OPTIONS+= ' -V -f bounces'
    from os import popen
    p = popen("%s -t %s" % (SENDMAIL, OPTIONS), "w")
    p.write("From: %s\n" % From)
    p.write("To: %s\n" % To)
    p.write("Subject: %s\n" % Subject)
    p.write("\n") # blank line separating headers from body
    p.write("%s\n" % Text)
    sts = p.close()
    if sts:
        print "Sendmail exit status", sts
