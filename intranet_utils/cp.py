#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
cp.py
 
    Fonctions pour cherrypy (intranet)
     
Copyright (c) 2006 by www.crans.org
"""
import cherrypy

def log(string, keyword = "INTRANET", level = 0):
    """Utilise la fonction log de cherrypy avec quelques modification
    
    -> introduit le login de la session en cours s'il existe
    """
    if not isinstance(string, unicode):
        try:
            string = string.decode("utf-8")
        except UnicodeDecodeError:
            # ça sera peut-être moche, mais ça peut pas fail
            string = string.decode("iso-8859-15")
    try:
        login = cherrypy.session['uid']
    except:
        pass
    else:
        string = u"[%s] %s" % ( login, string )
    string = string.encode("utf-8")
    cherrypy.log(string, keyword, level)
