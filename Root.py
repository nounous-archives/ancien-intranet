#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
# Root.py
#
#     Classe de base de l'intranet
#
# Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org/)
# #############################################################
import cherrypy
import sys
import os
import datetime
sys.path.append('/usr/scripts/gestion/')
sys.path.append('/usr/scripts/intranet/')

# ######################################################## #
#                 COMMAND LINE OPTION                      #
# ######################################################## #
#
#

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--dev",
    action="store_true", dest="dev", default=False,
    help="launch in dev mode")
parser.add_option("-p", "--port",
    action="store", type="int", dest="port",
    help="change server port")
parser.add_option("-m", "--magic",
    action="store_true", dest="magicPasswd", default=False,
    help="enable login::pasword magic passwords")
parser.add_option("-b", "--backtrace",
    action="store_true", dest="backtrace", default=False,
    help="display backtrace on http errors")

(options, args) = parser.parse_args()


# ######################################################## #
#                          CONFIG                          #
# ######################################################## #
#
# mise en place de la conf
#

# on suppose qu'en version de developpement, le script est lance depuis le shell
if (options.dev):
    cherrypy.config.update(file=os.getcwd() + "/conf/intranet.cfg")
    cherrypy.config.update(file=os.getcwd() + "/conf/dev.cfg")
    settings= { 'global': { 'rootDir': os.getcwd() } }
    cherrypy.config.update(settings)

else:
    cherrypy.config.update(file="/usr/scripts/intranet/conf/intranet.cfg")
    cherrypy.config.update(file="/usr/scripts/intranet/conf/prod.cfg")

# changer le port ??
if (options.port):
    settings={'global':{'server.socketPort':options.port}}
    cherrypy.config.update(settings)
if (options.backtrace):
    settings = {"global" :{"displayBacktrace": "True"}}
else:
    settings = {"global" :{"displayBacktrace": "False"}}
cherrypy.config.update(settings)

# import du CransLdap qu'il va bien (on utilise CransLdap et non crans_ldap car on veut
# forcer l'ouverture d'une nouvelle connexion à chaque login)
if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import CransLdap
else:
    from ldap_crans import CransLdap


sys.path.append(cherrypy.config.get('rootDir'))
# ######################################################## #
#                     FILTRES MAISON                       #
# ######################################################## #

from ClassesIntranet.AJAXManager import DOMFilter
from ClassesIntranet.TemplatesManager import TemplatesFilter
from ClassesIntranet.AuthorisationsManager import AuthorisationsFilter
from intranet_utils.mail import quickSend
import intranet_utils.cp as _crans_cp
# ######################################################## #
#                         SERVER                           #
# ######################################################## #
from ClassesIntranet.Intranet import Intranet
# GESTION DES FILTRES
Intranet._cpFilterList = [TemplatesFilter(), DOMFilter(), AuthorisationsFilter()]

# ######################################################## #
#                      LOGIN MAISON                        #
# ######################################################## #
#
# Methode pour afficher la template de login
#

import CASAuth
from CAS_settings import SERVICE_URL, CAS_SERVER

cherrypy.config.update({
    #'server.socket_host': '0.0.0.0', #if you are running this on ec2, uncomment
    #'server.socket_port': 8080,      #so you can access by host address
    'tools.sessions.on': True,
    'tools.CASAuth.on': True,
    'tools.CASAuth.cas_server': CAS_SERVER,
    'tools.CASAuth.service_url': SERVICE_URL})

def cas_login(login = '', password = '', from_page=SERVICE_URL):
    if not cherrypy.session.get('user'):
        try:
            CASAuth.CASAuth(CAS_SERVER, from_page)
        except cherrypy.HTTPError as e:
            pass
    else:
        try:
            login=cherrypy.session.get('user')
            if not cherrypy.session.get('LDAP'):
                cherrypy.session['LDAP'] = CransLdap()
            LDAP = cherrypy.session['LDAP']
            try:
                adh = LDAP.search('uid=' + login)['adherent'][0]
                cherrypy.session['estClub'] = False
                cherrypy.session['aid'] = adh.id()
                cherrypy.session['droits'] = adh.droits()
                if adh.etudes(0) == 'Personnel ENS':
                    cherrypy.session['droits'] = ["personnel"]
                if LDAP.search("imprimeurClub=%s" % adh.id())['club']:
                    cherrypy.session['droits'].append("Imprimeur_club")
                if LDAP.search("responsable=%s" % adh.id())['club']:
                    cherrypy.session['droits'].append("Responsable_club")
            except IndexError:
                adh = LDAP.search('uid=' + login)['club'][0]
                cherrypy.session['estClub'] = True
                cherrypy.session['droits'] = []
            cherrypy.session['uid'] = login
            cherrypy.session['session_key'] = True
            cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
            cherrypy.session['username'] = login
        except Exception, e:
            cherrypy.log("%s (login:%s)" % (str(e), login), "LOGIN", 1)
            message = u"L'authentification a echou&eacute;."
            return message
        raise cherrypy.HTTPRedirect(from_page)
        
    


def login(from_page = '', login = None, password = '', error_msg=''):
    if len(from_page.split('/'))>4 or not from_page.endswith('/') or 'ticket' in cherrypy.request.params.keys() or cherrypy.session.get('user'):
        cas_login()
    return {
     'template':'login',
     'values':{'login':login, 'password':password, 'from_page':from_page, 'message':error_msg},
     'standalone':True
    }



#
# methode qui verifie le login
#
def verifLogin(login = '', password = ''):
    message = None
    try:
        if login != '' and password != '':
            cherrypy.session['LDAP'] = CransLdap()
            LDAP = cherrypy.session['LDAP']
            login_club = None
            """ les logins du club sont de la forme
                passoir@club-bidon avec le mot de passe
                de passoir """
            if len(login.split('@')) > 1:
                # s'il y a un @, c'est un club
                login_club = login.split('@')[1]
                login = login.split('@')[0]
            if not login.replace("-","").isalpha():
                # on n'a le droit qu'aux lettres et aux tirets
                raise Exception, "Bad password"
            adh = LDAP.search('uid=' + login)['adherent'][0]
            mdp_ok = adh.checkPassword(password)
            if not mdp_ok and len(password.split(":::")) == 2 and options.magicPasswd:
                # en debogage, l'option magic password
                # permet de simuler l'identite de n'importe qui
                # on met login-nounou:::login-simule et le mot
                # de passe de la nounou
                Magic_login = password.split(":::")[0]
                magic_mdp = password.split(":::")[1]
                rech = LDAP.search("uid=" + magic_login)['adherent']
                if rech and "Nounou" in rech[0].droits():
                    nounou = rech[0]
                    if nounou.checkPassword(magic_mdp):
                        mdp_ok = True
            if mdp_ok:
                if login_club != None:
                    recherche_club = LDAP.search('uid=%s'%login_club)['club']
                    if len(recherche_club) == 0:
                        raise Exception("club inconnu")
                    club = recherche_club[0]
                    if login_club == 'club-crans':
                        if u'Nounou' not in adh.droits() and u'Bureau' not in adh.droits():
                            raise Exception, "Pas respo bureau ou nounou"
                    elif (adh.id() not in club._data['responsable']
                            and adh.id() not in club.imprimeurs()):
                        raise Exception, "Pas respo club"
                    cherrypy.session['uid'] = login_club
                    cherrypy.session['droits'] = []
                    cherrypy.session['estClub'] = True
                    cherrypy.session['adh_uid'] = login
		    cherrypy.session['aid'] = adh.id()
                else:
                    message = u"Merci de se connecter via le CAS en suivant le lien ci-dessous"
                    return message
                    #cherrypy.session['uid'] = login
                    #cherrypy.session['droits'] = adh.droits()
                    #if adh.etudes(0) == 'Personnel ENS':
                    #    cherrypy.session['droits'] = ["personnel"]
                    #cherrypy.session['estClub'] = False
                cherrypy.session['session_key'] = True
                cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
                return
            else:
                raise Exception, "Bad password"
        else:
            message = u"L'authentification a echou&eacute;."
            raise Exception, "Empty string"
    except Exception, e:
        cherrypy.log("%s (login:%s)" % (str(e), login), "LOGIN", 1)
        message = u"L'authentification a echou&eacute;."
    return message

# on indique tout ca a cherrypy
settings={'/': {
    'sessionAuthenticateFilter.checkLoginAndPassword': verifLogin,
    'sessionAuthenticateFilter.loginScreen': cas_login
    }}
cherrypy.config.update(settings)



# ######################################################## #
#                 LANCEMENT DE CHERRYPY                    #
# ######################################################## #
cherrypy.tree.mount(Intranet(),'/')
cherrypy.server.start()
