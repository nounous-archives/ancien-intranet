#! /usr/bin/env python
# -*- coding: iso8859-15 -*-

""" Script de callback de paypal. Ce script est rappel�
par paypal lorsque ce dernier a effectivement re�u le paiement. � nous
de valider alors la facture et cr�diter l'adh�rent.
Pour �tre pr�cis, paypal acc�de � l'url https://factures.crans.org/nip.py
(modifier l'url est possible depuis notre compte paypal)
Les d�tails des param�tres que ce script doit r�cup�rer sont plus bas"""

import urllib, httplib
import os, sys, traceback
sys.path.append('/usr/scripts/gestion/')
sys.path.append('/usr/scripts/')
sys.path.append('/usr/scripts/intranet')
import syslog
import cgi
from intranet_utils import mail
import cranslib.utils.exceptions
utils = cranslib.utils
###################################################################
#                     Fonction auxiliaires                        #
###################################################################

########################
# log
########################
# Log les messages
# permet de loguer au bon endroit
#
def log(aString):
    # print(aString)
    syslog.openlog('paypalNip')
    syslog.syslog(aString)

########################
# confirmNip
########################
#
#    - demande la confirmation du NIP a paypal
#    - cgiFieldStorage : les champs recus de paypal
#              au format cgi.FieldStorage
#    - saadbox : si vrai, utilise la version de test du site paypal
#
def confirmNip(cgiFieldStorage, sandbox = False ):

    # construction des champs a retourner a paypal
    nipVars = {'cmd':'_notify-validate'}

    for aKey in cgiFieldStorage.keys():
        nipVars[aKey] = cgiFieldStorage[aKey].value

    params = urllib.urlencode(nipVars)

    # headers a envoyer
    headers = {"Content-type": "application/x-www-form-urlencoded"}

    # ouverture de la connexion avec la bonne adresse
    if sandbox:
        conn = httplib.HTTPSConnection("www.sandbox.paypal.com")
        log("[txn_id %s] Using sandbox" % cgiFieldStorage.getvalue('txn_id'))
    else:
        conn = httplib.HTTPSConnection("www.paypal.com")
    conn.request("POST", "/cgi-bin/webscr", params, headers)
    response = conn.getresponse()
    data = response.read()
    conn.close()
    return data

class NipError(Exception):
    pass

########################
# sendDigest
########################
#
# - envoie un mail pour resumer ce qui s'est passs�...
def sendDigest(traceback = 0, **kw):
    # on r�cup�re le traceback
    tb = utils.exceptions.formatExc()

    # corps du message
    corps_message = "Bonsoir,\nPaypal a envoye une notification de paiement.\n"
    corps_message = "\n%s\n" % "\n".join( [ "%s: %s" % (str(a), str(kw[a])) for a in kw] )
    if traceback:
        corps_message += "\n------\n"
        corps_message += tb
    corps_message += "\n-- \nnip.py"
    mail.quickSend(From="paypal@crans.org",
      To="intranet-bugreport@lists.crans.org",
      Subject="Paiement Paypal",
      Text=corps_message)

###################################################################
#                     Debut du script                             #
###################################################################
# on imprime un entete pour pas avoir d'erreur 500
print "Content-type: text/html\n\n"

# on recupere les champs de paypal
# il fut garder les champs vide sinon paypal pas content...
form = cgi.FieldStorage(keep_blank_values=1)

log("[txn_id %s] NID reception" % form.getvalue('txn_id'))

# on teste si on est en dev ou pas
# (si le nip vient du sandbox, on est en dev)
if form.getvalue('test_ipn',0):
    from ldap_crans_test import crans_ldap
    paypalResp = confirmNip(form, True)
else:
    from ldap_crans import crans_ldap
    paypalResp = confirmNip(form)
LDAP = crans_ldap()

log("[txn_id %s] Paypal verification response : %s" % (form.getvalue('txn_id'), paypalResp))

try:
    if paypalResp != 'VERIFIED':
        raise NipError, "paypal return : %s" % paypalResp
    if form.getvalue('payment_status', 'na') != 'Completed':
        raise NipError, "payment pas fini (status : %s)" % form.getvalue('payment_status', 'n/a')

    # trouver la facture correspondante
    fid = form.getvalue('invoice', 0)
    if not fid:
        raise NipError, "fid introuvable"
    try:
        facture = LDAP.search("fid=%s" % str(fid), 'w')['facture'][0]
    except:
        raise NipError, "Facture introuvable"

    # verifier le montant
    devisePaypal = form.getvalue('mc_currency', 'na')
    if devisePaypal != 'EUR':
        raise NipError, "Mauvaise devise"
    montantPaypal = form.getvalue('mc_gross', 0)
    if facture.total() != float(montantPaypal):
        raise NipError, "Montant incorect"

    # crediter
    facture.recuPaiement('intranet')
    facture.save()

    sendDigest(
      txn_id = form.getvalue('txn_id'),
      montant = form.getvalue('mc_gross', 0),
      fid = form.getvalue('invoice', 0),
      paypal_verification = paypalResp,
      facture_creditee = "oui",
      sandbox = form.getvalue('test_ipn',0)
      )


except Exception, e:
    log("[txn_id %s] ERROR : %s" % (form.getvalue('txn_id'), str(e)))
    sendDigest(
       txn_id = form.getvalue('txn_id'),
       montant = form.getvalue('mc_gross', 0),
       fid = form.getvalue('invoice', 0),
       facture_creditee = "probablement pas",
       paypal_verification = paypalResp,
       sandbox = form.getvalue('test_ipn',0),
       erreur = str(e),
       traceback = 1
       )

