from ClassesIntranet.ModuleBase import ModuleBase
import cherrypy

class main(ModuleBase):
    _droits = ["Nounous"]
    def category(self):
        return "Beta"
    def title(self):
        return "Cr@ns LDAP"
    def icon(self):
       return "icon.png"

    def index(self):
        return {'template': 'gestion',
                'scripts':['org.crans.intranet.gestion.nocache.js'],
                'no-scripts':True,
                'values':{},}
    index.exposed = True

    def search(self, q):
        
        results = cherrypy.session['LDAP'].search( q )
        JSONResults = {}
        JSONAdherents = []
        for anAdh in results['adherent']:
            JSONAdherents.append({'Nom':anAdh.Nom(), 'id':anAdh.id()})
        JSONResults['adherents'] = JSONAdherents
        JSONMachines = []
        for aMach in results['machine']:
            JSONMachines.append( {'Nom':aMach.Nom(), 'id': aMach.id() } )
        JSONResults['machines'] = JSONMachines
        
        return JSONResults
    search.exposed = True
