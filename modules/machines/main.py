#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy, sys, os, datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Mes Machines"
    def icon(self):
       return "machines_icon.png"
    def category(self):
        return "Personnel"

    _club = True

    def index(self):
        raise cherrypy.HTTPRedirect('https://intranet2.crans.org/machines/')
    index.exposed = True
