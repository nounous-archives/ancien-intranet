#! /usr/bin/env python
# -*- coding: utf-8 -*-
# ##################################################################################################### #
#  page perso
# ##################################################################################################### #
#   Description:
#    
#   Informations:
# 
#   Pages:
#    
#
# ##################################################################################################### #

import cherrypy
import sys
import os
import datetime
# libraries crans
sys.path.append('/usr/scripts')
from utils.pagesperso import PagePerso

if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import *
#    print("monCompte : unsing test ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])
else:
    from ldap_crans import *
#    print("monCompte : unsing prod ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])

from ClassesIntranet.ModuleBase import ModuleBase

class main(ModuleBase):
    _droits = ["Nounous"]
    def category(self):
        return "Beta"
    def title(self):
        return "Page Perso"
    def index( self, message = '', error = '', field_sitename = None, field_slogan = None, field_data_changed = None):
        adh_pp = PagePerso( cherrypy.session['uid'] )
        if field_data_changed:
            if field_sitename:
                adh_pp.setNom( field_sitename )
            if field_slogan:
                adh_pp.setSlogan( field_slogan )
            adh_pp.save()
        t = {}
        t['message'] = message
        t['error'] = error
        t['sitename'] = adh_pp.nom()
        t['slogan'] = adh_pp.slogan()
        t['logourl'] = adh_pp.logo()
        return {
                'template'      :'index',
                'values'        :t,
                'stylesheets'   :['pageperso.css'],
                'scripts':['crans_domtab.js'],
            }
    index.exposed = True
