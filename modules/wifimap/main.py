#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Carte WiFi"
    def icon(self):
       return "icone_wifimap.png"
    def category(self):
        return "Services"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://intranet2.crans.org/wifimap/')
    index.exposed = True

