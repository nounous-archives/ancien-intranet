#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy, sys, os, datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["Nounou", "Apprenti", "Cableur"]
    def title(self):
       return "Prises réseau"
    def icon(self):
       return "prises_icon.png"
    def category(self):
        return "Personnel"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://intranet2.crans.org/prises/')
    index.exposed = True
