#! /usr/bin/env python
import cherrypy, tempfile, shutil, os, sys

sys.path.append('/usr/scripts/intranet')
from intranet_utils import cp
sys.path.append('/usr/scripts/impression')
import digicode
from ClassesIntranet.ModuleBase import ModuleBase


class main(ModuleBase):
    _droits = ["Imprimeur"]
    def category(self):
        return "Imprimeur"
    def title(self):
       return "Digicode"

    ##########################
    # affichage
    ##########################
    #
    # methode qui affiche la template
    #
    def index(self, submit = None, fileList = None, newFile = None ):
        return {'template':'digicode',
                'values':{},
                'stylesheets':['digicode.css'],
                'scripts':['digicode.js'],
            }
    index.exposed = True


    ##########################
    # AJAX
    ##########################
    #
    # methode qui renvoie la liste des codes
    #
    def codeList(self):
        try:
            listeBrute = digicode.list_code()
            liste_formatee = []
            for aCode in listeBrute:
                age = int(aCode[1])
                age_jours = (age/3600)/24
                age_heures = (age/3600) - age_jours*24
                age_minutes = (age/60) - (age/3600)*60
                if age_jours > 0:
                    age_string = "%sj %sh %smin" % (str(age_jours), str(age_heures), str( age_minutes ))
                elif age_heures > 0:
                    age_string = "%sh %smin" % (str(age_heures), str( age_minutes ))
                else:
                    age_string = "%smin" % (str( age_minutes ))
                liste_formatee.append({'code':aCode[0], 'age':age_string, 'desc':aCode[2]})
            return {'codes': liste_formatee}
        except Exception, e:
            cp.log('erreur lors de la creation de la liste de codes :' + str(e), 'DIGICODE', 1)
            return {'erreur':str(e)}
    codeList.exposed= True

    #
    # methode qui cree un code
    #
    def createCode(self, code=None, adherent=''):
        try:
            if adherent == '':
                adherent = cherrypy.session['uid']
            if code:
                try:
                    int(code)
                    if code.__len__() != 6:
                        raise
                except:
                    return {'formatErreur':1}
                code = digicode.save_code(code, adherent)
            else:
                code = digicode.gen_code(adherent)
            cp.log("code cree : %s" % code, 'DIGICODE')
            return {'code': code, "age" : "new", "desc":adherent}
        except Exception, e:
            cp.log("erreur lors de la creation de code : " + str(e), 'DIGICODE', 1)
            return {'erreur':str(e)}
    createCode.exposed= True
