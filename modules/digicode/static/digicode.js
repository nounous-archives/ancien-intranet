/* ************************************************************
 * Digicode
 ************************************************************
 * Digicode.init 		  : initialisation de la page
 * Digicode.makeCode    : creation de codes
 * Digicode.codelist    : liste des codes
 * Digicode.AJAX     	  : ajax
 */
Digicode = {};

/*****************************
    Digicode.init
 *****************************/
Digicode.init = function()
{
   // afficher le tableau codelist
   appendChildNodes("globalDiv", Digicode.codelist.create());
   // recuperer la liste des codes
   this.codelist.load();
   // afficher le formulaire de creation de code
   appendChildNodes("globalDiv", DIV({"id":"addCodeBox"}));
   Digicode.makeCode.displayForm();
}

/*****************************
    Digicode.makeCode
 *****************************/
Digicode.makeCode = {}

Digicode.makeCode.displayForm = function() 
{
   var myForm = FORM({'id':'createCodeForm', 'name':'createCodeForm','onsubmit':"Digicode.makeCode.createCode(document.createCodeForm.newCodeLogin.value, document.createCodeForm.newCode.value); return false;", 'style':'display: none;'},
		     LABEL({'for':'newCodeLogin'}, "Login adhérent :"),
		     INPUT({"name":"newCodeLogin", "size":"10", "maxlength":"20", "class":"textinput"}),
		     BR(),
		     LABEL({'for':'newCode'}, "Code :"),
		     INPUT({"name":"newCode", "size":"6", "maxlength":"6", "class":"textinput"}),
		     BUTTON({"type":"submit","onclick":"Digicode.makeCode.createCode(document.createCodeForm.newCodeLogin.value, document.createCodeForm.newCode.value); return false;", "style":"float:right;"},"Créer code"),
		     BUTTON({"type":"button","onclick":"Digicode.makeCode.createCode(document.createCodeForm.newCodeLogin.value)", "style":"float:right;"},"Code aléatoire")
		     );
   replaceChildNodes("addCodeBox", H1({},"Nouveau code"), myForm );
   appear(myForm);
   
}

Digicode.makeCode.restoreForm = function() 
{
   try 
     {
	removeElement("newCodeDisplay");
	removeElement("loading");
     } catch (error){}
   appear("createCodeForm");
   
}

Digicode.makeCode.disableForm = function() 
{
   try 
     {
	var form = getElement("createCodeForm");
	var elts = form.elements;
	for (i=0 ; i < elts.length ; i++)
	  elts[i].disabled = true;
     }
   catch (error){}
}

Digicode.makeCode.createCode = function(login, code)
{
   var image = createDOM("IMG",{'style':'margin-right:2px;float:right;','src':'/static/images/indicator.gif'});
   appendChildNodes("addCodeBox", DIV({'id':"loading",'style':'display:none;max-height:1em;float:left;'},image,"Loading"));
   this.disableForm();
   //removeElement("createCodeForm");
   appear("loading");
   if (code)
     Digicode.AJAX.call("createCode?code="+code + "&adherent=" + login, this.handleNewCode);
   else
     Digicode.AJAX.call("createCode?adherent=" + login, this.handleNewCode);
}

Digicode.makeCode.handleNewCode = function(res)
{
   if (res.code)
     {
	replaceChildNodes("addCodeBox",
			  H1({}, "Code créé"),
			  DIV({'id':"newCodeDisplay",
			      'style':'display:none;font-size:2em;maring:1em;font-weight:bold;text-align:center;',
			      'onclick':"Digicode.makeCode.displayForm();"},res.code));
	appear("newCodeDisplay");
	//removeElement("loading");
	Digicode.codelist.addCode(res);
     } else if (res.erreur) {
	logError("Erreur distante : " + res.erreur);
	alert("Erreur sur le serveur, le code est peut-être déjà pris.")
	  Digicode.makeCode.displayForm();
     } else if (res.formatErreur) {
	alert("Ceci n'est pas un code valide");
	Digicode.makeCode.displayForm();
     }
}

/*****************************
 Digicode.codelist
 *****************************/
Digicode.codelist = {};

Digicode.codelist.create = function ()
{
   var thead = createDOM("thead", {},
			 createDOM("th", {'class':'code'}, "Code" ),
			 createDOM("th", {'class':'age'}, "Age")
			 );
   var tbody = createDOM("tbody", {"id":"codeList"});
   return TABLE({"id":"codesTable", "cellspacing":"0"}, thead, tbody)
}



Digicode.codelist.load = function()
{
   Digicode.AJAX.call("codeList", Digicode.codelist.displayCodes);
}

Digicode.codelist.displayCodes = function(result)
{
   if (result.codes)
     replaceChildNodes('codeList',map(Digicode.codelist.newCodeTrNodeFromDict,result.codes));
   else if (result.erreur)
     logError("Erreur distante : " + result.erreur);
}

Digicode.codelist.newCodeTrNodeFromDict = function (aDict, style)
{
   if (style) {
      var aRow =  createDOM("TR", {'id':'code'+aDict.code,"style":style});
   } else 
     var aRow =  createDOM("TR", {'id':'code'+aDict.code});
   appendChildNodes(aRow,  createDOM("TD", {'class':'code'}, aDict.code, SPAN({'style':'color:gray;margin-left:2em;'}, aDict.desc ) ) );
   appendChildNodes(aRow,  createDOM("TD", {'class':'age'}, aDict.age) );
   return aRow;
}

Digicode.codelist.addCode = function (aDict)
{
   var newLine = this.newCodeTrNodeFromDict(aDict);
   appendChildNodes("codeList", newLine);
   pulsate(newLine);
}


/*****************************
 Digicode.AJAX
 *****************************/
Digicode.AJAX = {}

Digicode.AJAX.call = AJAX.call

setInterval(Digicode.codelist.load, 30000);
