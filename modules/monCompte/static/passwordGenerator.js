/************************************************************************
        Password generator
 ************************************************************************/

function GeneratePassword() {
    
    if (parseInt(navigator.appVersion) <= 3) { 
        alert("Sorry this only works in 4.0 browsers"); 
        return false; 
    }
    
    var length=8;
    var sPassword = "";
    length = 10;//document.aForm.charLen.options[document.aForm.charLen.selectedIndex].value;
    
    var noPunction = false;//(document.aForm.punc.checked);
    var randomLength = true;//(document.aForm.rLen.checked);
    
    if (randomLength) { 
        length = Math.random(); 
        
        length = parseInt(length * 100);
        length = (length % 7) + 6
    }
    
    
    for (i=0; i < length; i++) {
    
        numI = getRandomNum();
        if (noPunction) { while (checkPunc(numI)) { numI = getRandomNum(); } }
        
        sPassword = sPassword + String.fromCharCode(numI);
    }
    
    //document.aForm.passField.value = sPassword
    c = prompt('Mot de passe généré : ', sPassword)
    if (c!= null) {
        document.changePasswordForm.nouveauPassword1.value = c
        document.changePasswordForm.nouveauPassword2.value = c
    }
    return false;
}

function getRandomNum() {
        
    // between 0 - 1
    var rndNum = Math.random()

    // rndNum from 0 - 1000    
    rndNum = parseInt(rndNum * 1000);

    // rndNum from 33 - 127        
    rndNum = (rndNum % 94) + 33;
            
    return rndNum;
}

function checkPunc(num) {
    
    if ((num >=33) && (num <=47)) { return true; }
    if ((num >=58) && (num <=64)) { return true; }    
    if ((num >=91) && (num <=96)) { return true; }
    if ((num >=123) && (num <=126)) { return true; }
    
    return false;
}
