function askForName(oldName) {
   var c = '';
   while ( c == '') 
     c = prompt("Votre nom :",oldName);
   if (c == null)
     return false;
   else
     window.location.href= 'changeNomAdherent?nouveauNom=' + c;
}


function askForSurname(oldSurname) {
   var c = '';
   while ( c == '') 
     c = prompt("Votre prénom :",oldSurname);
   if (c == null)
     return false;
   else
     window.location.href= 'changePrenomAdherent?nouveauPrenom=' + c;
}

function askForTel(oldTel) {
   var c = '';
   while ( c == '') 
     c = prompt("Votre numéro de téléphone :",oldTel);
   if (c == null)
     return false;
   else
     window.location.href= 'changeTelAdherent?nouveauTel=' + c;
}


function newAlias() {
   var c = '';
   while ( c == '') 
     c = prompt("Nouvel alias :");
   if (c == null)
     return false;
   else
     window.location.href= 'newAlias?alias=' + c + "#mailTab";   
}
