#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-
# #########################################################################
#  MonCompte
# #########################################################################
#   Description:
#    
#   Informations:
# 
#   Pages:
#    
#
# #########################################################################

import cherrypy
import sys
import os
import datetime
# libraries crans
sys.path.append('/usr/scripts/intranet')
from intranet_utils import cp as _crans_cp
sys.path.append('/usr/scripts/gestion/')
from config_mail import MailConfig

if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import *
#    print("monCompte : unsing test ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])
else:
    from ldap_crans import *
#    print("monCompte : unsing prod ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])

from ClassesIntranet.ModuleBase import ModuleBase

class main(ModuleBase):
    def category(self):
        return "Services"
    def title(self):
       return "Rechargement Paypal"

    _club = False
    _adh = False

    def getCurrentAdministrativeYear(self):
        '''
            premiere partie de l''annee scolaire en cours
            ex : le 5 juin 2006 retourne 2005
        '''
        now = datetime.datetime.now()
        currentYear = int(now.strftime("%Y"))
        currentMonth = int(now.strftime("%m"))
        if currentMonth > 8:
            administrativeYear = currentYear
        else:
            administrativeYear = currentYear - 1
        return administrativeYear

    ##########################
    # affichage
    ##########################
    # 
    # methode qui n'affiche rien et renvoie directement au rechargement
    #
    def index(self, message = '', error = '', currentTab = 'mainTab', etape='1', combien=None):
         return self.rechargerCompteImpression(etape, combien )
    index.exposed = True



    ##########################
    # paypal
    ##########################
    #
    # methode qui affiche successivement les
    # templates du popup pour  recharger son compte impression
    # via paypal
    #
    def rechargerCompteImpression(self, etape = '1', combien = None):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        if (etape == "1"):		# Introduction
            return {
                'template'      :'MonCompteRechargePaypal1',
                'standalone'	:True,
                'values'        :{},
            }
        elif (etape == "2"):	# choix montant
            return {
                'template'      :'MonCompteRechargePaypal2',
                'standalone'	:True,
                'values'        :{},
            }
        elif (etape == "3"):	# confirmer facture
            # creer objet facture
            f = Facture(adh)
            # /!\ verifier que combien est un nombre
            #     et qu'il n'y a pas plus de 2 chiffres apres le point...
            #     (ce serait bien aussi si on pouvait mettre une virgue a la place du point)
            try:
                # remplacage des virgules
                combien = combien.replace(u',', u'.')
                # convertissage
                combien = float(combien)
                # arrondissage-tronquage :
                combien = float(int(combien*100)/100.0)
                # nombre positif
                if combien < 0:
                	raise ValueError
            except Exception:
                return {
                    'template'      :'MonCompteRechargePaypal2', 
                    'standalone'	:True,
                    'values'        :{'error':"Le montant doit &ecirc;tre un nombre positif !", 'combien':combien},
                }
            f.ajoute({'nombre': 1, 'code': 'SOLDE', 'designation': 'Credit du compte impression (intranet)', 'pu': combien})
            cherrypy.session['freshFacture'] = f
            pageData = {}
            pageData['details'] = [
            {
            'intitule':art['designation'],
            'quantite':art['nombre'],
            'prixUnitaire':art['pu'],
            'prixTotal':art['pu']*art['nombre'],
            } for art in f.articles()]
            pageData['total'] = f.total()
            return {
                'template'      :'MonCompteRechargePaypal3', 
                'standalone'	:True,
                'values'        :pageData,
            }
        elif (etape == "4"):# payer maintenant ?
            # sauver objet facture
            f = cherrypy.session['freshFacture']
            f.save()
            return {
                'template'      :'MonCompteRechargePaypal4', 
                'standalone'	:True,
                'values'        :{'lienPaypal' : f.urlPaypal(useSandbox = cherrypy.config.get("paypal.useSandbox", False), 
                							businessMail = cherrypy.config.get("paypal.businessAdress", "paypal@crans.org"),
                							return_page = "https://intranet.crans.org/monCompte/paypalReturn",
                							cancel_return_page = "https://intranet.crans.org/monCompte/paypalCancel",
                							)},
            }
    rechargerCompteImpression.exposed = True
    
    def paypalReturn(self, **kw):
        _crans_cp.log("retour de paypal avec les infos : %s" % " ".join( [ "[%s: %s]" % (str(a), str(kw[a])) for a in kw] ) )
        return {
          'template'      :'MonComptePaypalReturn', 
          'standalone'	:True,
          'values'        :{},
        }
    paypalReturn.exposed = True

    def paypalCancel(self, **kw):
        _crans_cp.log("annulation de paypal avec les infos : %s" % " ".join( [ "[%s: %s]" % (str(a), str(kw[a])) for a in kw] ) )
        return {
                'template'      :'MonComptePaypalCancel', 
                'standalone'	:True,
                'values'        :{},
            }
    paypalCancel.exposed = True

