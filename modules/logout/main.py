#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
#from Root import CAS_SERVER
# libraries crans
sys.path.append('/usr/scripts/intranet/')
import intranet_utils.cp as _crans_cp
from ClassesIntranet.ModuleBase import ModuleBase
from CAS_settings import SERVICE_URL, CAS_SERVER
class main(ModuleBase):
    _droits = []
    def title(self):
       return "CAS Logout"

    def index(self):
        if cherrypy.session['estClub'] and 'aid' in cherrypy.session.keys():
            LDAP = cherrypy.session['LDAP']
            adh = LDAP.search('aid=%s' % cherrypy.session['aid'])['adherent'][0]
            cherrypy.session['uid'] = adh.compte()
            cherrypy.session['droits'] = adh.droits()
            if adh.etudes(0) == 'Personnel ENS':
                cherrypy.session['droits'] = ["personnel"]
            cherrypy.session['estClub'] = False
            cherrypy.session['session_key'] = True
            cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
            cherrypy.session['username'] = adh.compte()
            raise cherrypy.HTTPRedirect('https://intranet.crans.org')

        id = cherrypy.session['_id']
        for key in cherrypy.session.keys():
            del(cherrypy.session[key])
        cherrypy.session['_id'] = id
        raise cherrypy.HTTPRedirect('%s/logout?url=%s' % (CAS_SERVER,SERVICE_URL))

    def accessible(self):
        return False
    index.exposed = True
