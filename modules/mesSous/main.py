#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-
# ########################################################################
#  Mes Sous
# ########################################################################
#   Description:
#     Affiche la liste des factures et l'historique de debits/credits de
#     l'adherent. Fait aussi les rechargements Paypal
#   Informations:
#
#   Pages:
#     index:liste des factures
#     historique: historique des débits/crédits
#     rechargementPaypal: Comme son nom l'indique
#
# ########################################################################
import cherrypy, sys, os, datetime


if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import *
#    print("monCompte : unsing test ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])
else:
    from ldap_crans import *
#    print("monCompte : unsing prod ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])


from ClassesIntranet.ModuleBase import ModuleBase


class main(ModuleBase):
    def title(self):
       return "Mon Solde"
    def category(self):
        return "Personnel"
    def icon(self):
       return "icon.png"
    _club = True


    def getCurrentAdministrativeYear(self):
        '''
            premiere partie de l''annee scolaire en cours
            ex : le 5 juin 2006 retourne 2005
        '''
        now = datetime.datetime.now()
        currentYear = int(now.strftime("%Y"))
        currentMonth = int(now.strftime("%m"))
        if currentMonth > 8:
            administrativeYear = currentYear
        else:
            administrativeYear = currentYear - 1
        return administrativeYear


    def index(self, message = '', error = ''):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        t = {}
        t['message'] = message
        t['error'] = error
        t['solde'] = adh.solde

        ##############  liste des factures  ##############
        listeFactures = []
        for f in adh.factures():
            if not f.modePaiement() == 'paypal':
                continue
            facture = {}
            facture['no'] = f.numero()
            facture['intitule'] = f.articles()[0]['designation']
            facture['details'] = [
                {
                    'intitule':art['designation'],
                    'quantite':art['nombre'],
                    'prixUnitaire':art['pu'],
                    'prixTotal':art['pu']*art['nombre'],
                } for art in f.articles()]
            facture['montant'] = f.total()
            facture['paypal'] = f.urlPaypal(useSandbox = cherrypy.config.get("paypal.useSandbox", False),

                            businessMail = cherrypy.config.get("paypal.businessAdress", "paypal@crans.org"),
                            return_page = "https://intranet.crans.org/monCompte/paypalReturn",
                            cancel_return_page = "https://intranet.crans.org/monCompte/paypalCancel",
                            )

            facture['payee'] = f.recuPaiement()
            listeFactures.append(facture)
        t['listeFactures'] = listeFactures

        return {
                'template'      :'factures',

                'values'        :t,
                'stylesheets'   :['cransFactures.css'],
                'scripts'       :[],
            }
    index.exposed = True

    def historique(self, page = 1, items_per_page = 20):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        lst = [ x for x in adh.historique() if x.split(u' : ',1)[1].startswith(u'credit') or x.split(u' : ',1)[1].startswith(u'debit') ]
        histLst = []
        for anItem in lst:
            aLine = {}
            aLine["date"] = anItem.split(u",")[0]
            aLine["type"] = anItem.split(u' : ',2)[1].split(u' ')[0]
            aLine["montant"] = anItem.split(u' : ',2)[1].split(u' ')[1]
            try:
                aLine["intitule"] = anItem.split(u'[')[1].split(u']')[0]
            except Exception:
                aLine["intitule"] = ""
            histLst.append(aLine)

        histLst.reverse()
        page = int(page)
        items_per_page = int(items_per_page)
        if page == 1:
            prevPage = None
        else:
            prevPage = page - 1

        if page * items_per_page >= histLst.__len__():
            nextPage = None
        else:
            nextPage = page + 1
        offset = items_per_page * ( page - 1)


        return {
                'template'      :'factures-historique',
                'values'        :{
                         'liste':lst,
                         'historic_items':histLst[offset:offset + items_per_page],
                         'nextPage':nextPage,
                         'prevPage':prevPage
                         },
                'stylesheets'   :['cransFactures.css'],
                'scripts'		:[],
            }
    historique.exposed = True

    def delFacture(self, no):
        try:
            # trrouver la factures
            fact = cherrypy.session['LDAP'].search('fid=' + no, 'w')['facture'][0]
            #verifier qu'elle appartient bien a l'adherent
            if not fact.proprietaire().compte() == cherrypy.session['uid']:
                raise Exception, "Impossible de supprimer cette facture %s != %s" % (fact.proprietaire().mail(), cherrypy.session['uid'])
            # la supprimer
            fact.delete()
        except Exception, e:
            cherrypy.log(str(e), "FACTURES", 1)
        return self.index()
    delFacture.exposed = True


    ##########################
    # paypal
    ##########################
    #
    # methode qui affiche successivement les
    # templates du popup pour  recharger son compte impression
    # via paypal
    #
    def rechargerCompteImpression(self, etape = '1', combien = None):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        if (etape == "1"):		# Introduction
            return {
                'template'      :'MonCompteRechargePaypal1',
                'standalone'	:True,
                'values'        :{},
            }
        elif (etape == "2"):	# choix montant
            return {
                'template'      :'MonCompteRechargePaypal2',
                'standalone'	:True,
                'values'        :{},
            }
        elif (etape == "3"):	# confirmer facture
            # creer objet facture
            f = Facture(adh)
            f.modePaiement('paypal')
            # /!\ verifier que combien est un nombre
            #     et qu'il n'y a pas plus de 2 chiffres apres le point...
            #     (ce serait bien aussi si on pouvait mettre une virgue a la place du point)
            try:
                # remplacage des virgules
                combien = combien.replace(u',', u'.')
                # convertissage
                combien = float(combien)
                # arrondissage-tronquage :
                combien = float(int(combien*100)/100.0)
                # nombre positif
                if combien < 0:
                	raise ValueError
            except Exception:
                return {
                    'template'      :'MonCompteRechargePaypal2',
                    'standalone'	:True,
                    'values'        :{'error':"Le montant doit &ecirc;tre un nombre positif !", 'combien':combien},
                }
            f.ajoute({'nombre': 1, 'code': 'SOLDE', 'designation': 'Credit du compte impression (intranet)', 'pu': combien})
            cherrypy.session['freshFacture'] = f
            pageData = {}
            pageData['details'] = [
            {
            'intitule':art['designation'],
            'quantite':art['nombre'],
            'prixUnitaire':art['pu'],
            'prixTotal':art['pu']*art['nombre'],
            } for art in f.articles()]
            pageData['total'] = f.total()
            return {
                'template'      :'MonCompteRechargePaypal3',
                'standalone'	:True,
                'values'        :pageData,
            }
        elif (etape == "4"):# payer maintenant ?
            # sauver objet facture
            f = cherrypy.session['freshFacture']
            f.save()
            return {
                'template'      :'MonCompteRechargePaypal4',
                'standalone'	:True,
                'values'        :{'lienPaypal' : f.urlPaypal(useSandbox = cherrypy.config.get("paypal.useSandbox", False),
                							businessMail = cherrypy.config.get("paypal.businessAdress", "paypal@crans.org"),
                							return_page = "https://intranet.crans.org/monCompte/paypalReturn",
                							cancel_return_page = "https://intranet.crans.org/monCompte/paypalCancel",
                							)},
            }
    rechargerCompteImpression.exposed = True

    def paypalReturn(self, **kw):
        _crans_cp.log("retour de paypal avec les infos : %s" % " ".join( [ "[%s: %s]" % (str(a), str(kw[a])) for a in kw] ) )
        return {
          'template'      :'MonComptePaypalReturn',
          'standalone'	:True,
          'values'        :{},
        }
    paypalReturn.exposed = True

    def paypalCancel(self, **kw):
        _crans_cp.log("annulation de paypal avec les infos : %s" % " ".join( [ "[%s: %s]" % (str(a), str(kw[a])) for a in kw] ) )
        return {
                'template'      :'MonComptePaypalCancel',
                'standalone'	:True,
                'values'        :{},
            }
    paypalCancel.exposed = True

