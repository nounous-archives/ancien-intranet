#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Webmail"
    def icon(self):
       return "roundcube_icon.png"
    def category(self):
        return "Services"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://roundcube.crans.org')
    index.exposed = True

