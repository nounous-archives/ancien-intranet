#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
#
#     interface d'impression
#
# Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org)
# #############################################################

import cherrypy
import tempfile
import shutil
import os
import commands
import sys
import re
from re import compile

if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

from intranet.intranet_utils import cp
from impression import digicode, etat_imprimante
from impression.impression_hp import impression
from impression.base import FichierInvalide,SoldeInsuffisant
from traceback import format_exception

FILE_UPLOAD_BASE_FOLDER = cherrypy.config.get('fileUpload.folder', "/var/impression/fichiers/")

class FileError(Exception):
    pass
from ClassesIntranet.ModuleBase import ModuleBase

def fmt_exc(e):
    return '<br/>'.join(format_exception(e))

class main(ModuleBase):
    def category(self):
        return "Services"
    def title(self):
        return "Impression"
    def icon(self):
        return "icon.png"
    _club = True

    ##########################
    # affichage
    ##########################
    #
    # template principale
    #
    def index(self, submit = None, fileList = None, newFile = None ):
        # <!> b2moo: interface en test
        #try:
            #adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
            #test = bool(set([u'Nounou', u'Imprimeur']).intersection(adh.droits()))
        #except:
            #test = False
        #if not test:
            #return {'template':'panne',
            #}
        # <!> remove this when test finished
        data = {}
        # on efface un eventuel objet impression existant
        cherrypy.session['impression'] = None
        if submit == "Envoyer":
            try:
                newFile.filename = re.sub("\s+", "_", newFile.filename)
                newFile.filename = re.sub("[^\w\._]", "", newFile.filename)
                self.savePDF(newFile)
                data['fileName'] = newFile.filename.encode('ascii','replace').replace('?','_')
            except FileError, e:
                data['openError'] = e.args[0]
        elif submit == "Choisir":
            if (fileList):
                data['fileName'] = fileList
            else:
                data['openError'] = "Choisissez un fichier"

        data['fileList'] = self.getUploadedFileListFor(cherrypy.session['uid'])
        try:
            etat_imprimante.etat()
        except Exception, e:
            data['Erreur_imprimante'] = str(e).replace("\"", "\\\"")
            data['errorMsg'] = u"Imprimante injoignable"
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        if not cherrypy.config.get('crans.activate', True):
            data['Erreur_imprimante'] = "Config impression"
            data['errorMsg'] = cherrypy.config.get('crans.activate.errorMsg', u"Imprimante HS")
        return {'template':'impression',
                'values':data,
                'stylesheets':['impression.css'],
                'scripts':['impression.js', 'popup.js'],
            }
    index.exposed = True

    ##########################
    # devis
    ##########################
    #
    # methode qui affiche la template du devis
    #

    def devis(self):
        if cherrypy.session.has_key('impression') and cherrypy.session['impression'] != None :
            return {
        'template':'impression-devis',
        'values':
            {
            'devis':cherrypy.session['impression'].devisDetaille(),
            'total':cherrypy.session['impression'].prix(),
            'nomFichier':cherrypy.session['impression'].fileName(),
            },
            'standalone':True,
            }
        else:
            return {
        'template':'impression-devis',
        'values': { },
        'standalone':True,
            }
    devis.exposed = True


    ##########################
    # AJAX
    ##########################
    #
    # methode qui renvoie la liste des codes de l'adherent
    #
    def codeList(self):
        try:
            listeBrute = digicode.list_code()
            # liste de la forme :
            # [(code, age, description),...]
            listeBrute = [item[0] for item in listeBrute if item[2] == cherrypy.session['uid']]
            return {'codes': listeBrute}
        except Exception, e:
            cp.log('erreur lors de la creation de la liste de codes :' + str(e), 'IMPRESSION', 1)
            return {'erreur':str(e)}
    codeList.exposed = True

    #
    # methode qui indique quel fichier utiliser
    #
    def useFile(self, fileName):
        try:
            filepath = os.path.join(os.path.join(FILE_UPLOAD_BASE_FOLDER,  cherrypy.session['uid']+"/"), fileName)
            adh = cherrypy.session['uid']
            if cherrypy.session['estClub']:
                adh = cherrypy.session['adh_uid'] + '@' + adh
            cherrypy.session['impression'] = impression(filepath, adh)
            cp.log("useFile returns: %s" % str( cherrypy.session['impression'].pages() ))
            return {'nbPages': cherrypy.session['impression'].pages()}
        except FichierInvalide, e:
            cp.log("useFile : %s (%s)" % (str(e), e.file()), 'IMPRESSION', 1)
            return {'erreur':str(e) }
        except Exception, e:
            cp.log("useFile : %s" % str(e), 'IMPRESSION', 1)
            return {'erreur':str(e) }
    useFile.exposed = True

    #
    # methode pour changer les parametres
    #
    def changeSettings(self, copies=None, couleur=None, recto_verso=None, agrafage=None, papier=None, livret=None):
        if not cherrypy.session.has_key('impression') or cherrypy.session['impression'] == None :
            return {'nouvPrix':0.0}
        try:
            nouvPrix = cherrypy.session['impression'].changeSettings(papier=papier, couleur=couleur, agrafage=agrafage, recto_verso=recto_verso, copies=int(copies), livret=livret)
            cp.log("changeSettings returns : %s" % str(nouvPrix))
        except Exception, e:
            cp.log("changeSettings : %s" % str(e), 'IMPRESSION', 1)
            return {"erreur":str(e)}
        return {'nouvPrix':nouvPrix}
    changeSettings.exposed = True


    #
    # methode pour lancer l'impression
    #
    def lancerImpression(self):
        try:
            cherrypy.session['impression'].imprime()
        except SoldeInsuffisant:
            return {"SoldeInsuffisant":1}
        except Exception, e:
            cp.log("lancerImpression : %s" % str(e), 'IMPRESSION', 1)
            return {"erreur":str(e)}
        cp.log("impression", 'IMPRESSION')
        return {
                'code':str(digicode.gen_code(cherrypy.session['uid'])) + "#",
                'code_bat_j': cherrypy.config.get('crans.impression.codes.batJ', u"Non disponible")
                }
    lancerImpression.exposed = True

    #
    # methode pour recuperer l'etat de l'imprimante
    #
    def etatImprimante(self):
        if not cherrypy.config.get('crans.activate', True):
            return {"printer_state"  : u"Syst�me down"}
        try:
            return {"printer_state"  : (etat_imprimante.etat())}
        except Exception, e:
            return {"printer_state"  : 'Imprimante hors ligne'}
    etatImprimante.exposed = True

    #
    # methode pour le solde
    #
    def AJAXGetSolde(self):
        try:
            adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
            return {"solde"  : adh.solde() }
        except Exception, e:
            return {"erreur" : str(e)}
    AJAXGetSolde.exposed = True




    ##########################
    # privees
    ##########################
    #
    # methode pour obtenir la liste des fichiers upload�s
    #
    def getUploadedFileListFor(self, adh):
        file_folder = os.path.join(FILE_UPLOAD_BASE_FOLDER,  cherrypy.session['uid']+"/")
        if not os.path.isdir(file_folder):
            return []
        liste = os.listdir(file_folder)
        list_pdf = []
        # exclusion des fichiers qui ne sont pas des PDF
        for f in liste:
            if commands.getoutput('file -ib %s' % commands.mk2arg(file_folder, f)) == "application/pdf":
                list_pdf.append(f)
        return list_pdf


    #
    # methode pour enregistrer un fichier
    #
    def savePDF(self, aFile):

        _, tempFileName = tempfile.mkstemp()
        f = open(tempFileName, 'w+b')
        size = 0
        while True:
            data = aFile.file.read(1024 * 8) # Read blocks of 8KB at a time
            if not data:
                break
            f.write(data)
            size += len(data)
        f.close()
        if size == 0:
            raise FileError("Fichier vide")

        file_folder = os.path.join(FILE_UPLOAD_BASE_FOLDER,  cherrypy.session['uid']+"/")
        if not os.path.isdir(file_folder):
            os.makedirs(file_folder, mode=0750)
        newFilePath = os.path.join(file_folder, aFile.filename.encode('ascii','replace').replace('?','_'))
        shutil.move(tempFileName, newFilePath)
        os.chmod(newFilePath, 0640)
        cp.log("New file uploaded at : %s" % newFilePath, "IMPRESSION")
        return newFilePath





