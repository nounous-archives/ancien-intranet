# t.py
# ----
#
# Copyright (C) 2007 Jeremie Dimino <jeremie@dimino.org>
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.

import os, commands

def getUploadedFileListFor():
    file_folder = '/var/impression/fichiers/dimino'
    if not os.path.isdir(file_folder):
        return []
    list = os.listdir(file_folder)
    list_pdf = []
    # exclusion des fichiers qui ne sont pas des PDF
    for f in list:
        print commands.getoutput('file -ib %s' % commands.mk2arg(file_folder, f))
        if commands.getoutput('file -ib %s' % commands.mk2arg(file_folder, f)) == "application/pdf":
            list_pdf.append(f)
    return list_pdf
