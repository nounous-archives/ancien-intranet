/* ************************************************************
 * Impression
 ************************************************************
 * Impression.settings : panneau de configuration
 * Impression.popup    : popup
 * Impression.AJAX     : ajax
 */
Impression = {};

/*****************************
    Impression.settings
 *****************************/

Impression.settings = {};

  //
//// images : images used for previewing
//
Impression.settings.images = [
			      "portrait_couleurs_agraphediagonale.png",
			      "portrait_couleurs_pasdagraphes.png",
			      "portrait_couleurs_uneagraphe.png",
			      "portrait_couleurs_Deuxagraphes.png",
			      "portrait_couleurs_troisAgraphes.png",
			      "portrait_couleurs_stitching.png",
			      "portrait_nb_agraphediagonale.png",
			      "portrait_nb_pasdagraphes.png",
			      "portrait_nb_uneagraphe.png",
			      "portrait_nb_Deuxagraphes.png",
			      "portrait_nb_troisAgraphes.png",
			      "portrait_nb_stitching.png",
			      "portrait_transparent_couleurs.png",
			      "portrait_transparent_nb.png",
			      ];
/*
Impression.settings.preloadImage = function(imageName) {
   var image = new Image();
   image.src = "./static/"+imageName;
}

Impression.settings.preloadAllImages = function() {
   log("Preloading images...");
   map(this.preloadImage,this.images);
}
*/
  //
//// init : parse every field and display preview
//
Impression.settings.init = function () {
   log("Init settings...");
   this.theform = document.form_impression;
   this.disableForm(false);
}
  //
//// reset : reset form and then re-init settings
//
Impression.settings.reset = function () {
   log("Reset form");
   this.theform.reset();
   this.init();
   Impression.settings.update();
}

Impression.settings.disableForm = function(bool)
{
   log("Set Disable Form : " + bool);
   var fields = this.theform.elements;
    for( var i=0; i< fields.length; i++)
     {
        this.setDisableField( fields[i], bool );
     }
}

  //
//// getValue : parse a field and store value in fielld.name
//
Impression.settings.getValue = function(field)
{
   if (field.value)
     {
	this[field.name] = field.value;
	log( field.name + " is now " + this[field.name]);
     }
   else
     {
	logError("Can't get fieldValue");
     }
}
  //
//// getCopies : parse copies field
//
Impression.settings.getCopies = function(field) {
   if ( (!field.value) || (parseInt(field.value)!=field.value-0) || (parseInt(field.value) < 1) )
     {
	this.copies = 1;
	logError("Can't get copies");
     }
   else
     {
	this.copies =  parseInt(field.value);
     }
   log("copies is now " + this.copies);
}

  //
//// setValue : set field value
//
Impression.settings.setValue = function(afield, avalue) {
   afield.value = avalue;
   this.getValue(afield);
}

  //
//// setDisableField : set field disabled on/off
//
Impression.settings.setDisableField = function( afield, isdisabled ) {
   afield.disabled = isdisabled ;
}

  //
//// disableField : set field disabled on
//
Impression.settings.disableField = function(afield) {
   this.setDisableField(afield, true);
}

  //
//// fieldChanges : when a field is changed
//
Impression.settings.update = function () {
   var orientation = "portrait";
   Impression.settings.livret="False"
   if (this.theform.type_impression_couleur.checked)
     this.getValue(this.theform.type_impression_couleur);
   else
     this.getValue(this.theform.type_impression_nb);

   if (this.theform.disposition_recto.checked)
     this.getValue(this.theform.disposition_recto);
   else if (this.theform.disposition_recto_verso.checked)
     this.getValue(this.theform.disposition_recto_verso);
   else
       {
	   this.getValue(this.theform.disposition_livret);
	   Impression.settings.livret="True";
       }
   this.getValue(this.theform.papier);
   this.getCopies(this.theform.nb_copies);
   this.getValue(this.theform.agrafage);
   // Contraintes
   if (Impression.settings.nb_pages > 60)
       { this.disableField(this.theform.disposition_livret);
	   this.theform.disposition_livret.checked = false }
   if ( ((this.papier == "A4") &&
	 (( (this.theform.disposition_recto.checked) && (Impression.settings.nb_pages>50) ) ||
	  ( (this.theform.disposition_recto_verso.checked) && (Impression.settings.nb_pages>100) ) ))
       ||
	((this.papier == "A3") &&
	 (( (this.theform.disposition_recto.checked) && (Impression.settings.nb_pages>30) ) ||
	  ( (this.theform.disposition_recto_verso.checked) && (Impression.settings.nb_pages>60) ) ))
       )
     {
	this.setValue(this.theform.agrafage, "None");
	this.disableField(this.theform.agrafage);
	this.getValue(this.theform.agrafage);
     } else {
	this.setDisableField(this.theform.agrafage, false);
     }
   if (this.papier == "A4tr")
     {
	this.theform.disposition_recto.checked = true;
	this.disableField(this.theform.disposition_recto);
	this.disableField(this.theform.disposition_recto_verso);
	this.getValue(this.theform.disposition_recto);
     }
   else
     {
	this.setDisableField(this.theform.disposition_recto, false);
	this.setDisableField(this.theform.disposition_recto_verso, false);
     }

   this.updatePreview();
   Impression.AJAX.recalcPrix();

}

  //
//// updatePreview : update preview with new value
//
Impression.settings.updatePreview = function()
{
   var image_name = "";
   if (this.papier == "A4tr")
     image_name = "portrait_transparent_" + this.type_impression + ".png";
   else
     image_name = "portrait_" + this.type_impression + '_' + this.agrafes + ".png";
   var image = createDOM("IMG",{'src':'./static/'+image_name});
   log("Updating preview (new image : " + image_name + ")");
   replaceChildNodes("preview",image)
}


/*****************************
 Impression.popup
 *****************************/
Impression.popup = {};

Impression.popup.popupImpression = function(code, codeJ) {
   Popup.hide();
   Popup.create({}, "Impression en cours...",
		DIV(
		    {"id":"printingPopupContent", "style":"background-image:url(./static/dialog-printer.png)"},
		    SPAN("code: "+code),
		    SPAN("Batiment J: "+codeJ),
		    A({"href":"https://wiki.crans.org/VieCrans/ImpressionReseau", "class":"aide", "target":"_blank"}, "Comment récupérer mon impression ? "),
		    A({"href":"index", "class":"aide", "style":"text-align:right;"}, "Nouvelle impression")
		)

   );
Popup.display();
}

Impression.popup.popupError = function(erreur) {
   Popup.hide();
   Popup.create({}, "Erreur",
		DIV({"id":"printingPopupContent", "style":"background-image:url(./static/dialog-warning.png)"},
		    SPAN(erreur),
		    A({"href":"mailto:nounous@crans.org", "class":"crans_help aide"}, "Envoyer un rapport aux nounous"),
		    Popup.closeLink({"class":"aide", "style":"text-align:right;"}, "Fermer")));
   Popup.display();
}

Impression.popup.popupSolde = function(code) {
   Popup.hide();
   Popup.create({}, "Solde insuffisant",
		DIV(
		    {"id":"printingPopupContent", "style":"background-image:url(./static/dialog-solde.png)"},
		    SPAN("pas assez de sous"),
		    A({"href":"https://wiki.crans.org/CransPratique/SoldeImpression", "class":"aide", "target":"_blank"}, "Comment recharger mon compte ? "),
		    Popup.closeLink({"class":"aide", "style":"text-align:right;"}, "Fermer")));
   Popup.display();
}

Impression.popup.popupCodes = function(codeList) {
   Popup.hide();
   Popup.create({}, "Codes de mes impressions",
		DIV(
		    {"id":"printingPopupContent", "style":"background-image:url(./static/dialog-lock.png)"},
		    SPAN("Mes codes"),
		    //UL({"size":"4", "style":"width:6em;; height:4em;padding:0;margin:0;list-style-type:none;overflow:auto;"}, map(function (code) {return LI({}, code);}, codeList)),
		    SELECT({"size":"4", "style":"width:100%;"}, map(function (code) {return OPTION({}, code);}, codeList)),
		    Popup.closeLink({"class":"aide", "style":"text-align:right;"}, "Fermer")));
   Popup.display();
}



/*****************************
 Impression.mesCodes
 *****************************/
Impression.mesCodes = {};
Impression.mesCodes.getCodes = function ()
{
   Impression.AJAX.call('codeList', this.displayCodes );
}

Impression.mesCodes.displayCodes = function (result) {
	if (result.codes)
		Impression.popup.popupCodes(result.codes);
	else if (result.erreur)
		logError('erreur distante (codeLlist) : ' + result.erreur);
	else
		logError('erreur bizarre (codeLlist) : ');
}

/*****************************
 Impression.AJAX
 *****************************/
Impression.AJAX = {};

Impression.AJAX.call = function(url,callback) {
  AJAX.call( url, callback, true);
}

Impression.AJAX.modifPrix = function( text, wheel )
{
   if (wheel)
     {
	var image = createDOM("IMG",{'src':'./static/indicator.gif'});
     }
   else
     {
	var image = new DIV({});
     }
   var item = new DIV({'onclick':'Impression.AJAX.recalcPrix();'}, image, text);
   replaceChildNodes("prix_placeholder",item);
   log( text );
}

Impression.AJAX.modifNbPages = function( nbpages )
{
   Impression.settings.nb_pages = nbpages;
   replaceChildNodes("fileNbPages",nbpages);
}

Impression.AJAX.usefile = function(filename) {
   var item = new DIV({}, "Calculs en cours...");
   this.modifPrix("Analyse du fichier...", true);
   this.call('useFile?fileName=' + filename,this.analysefini );
}

Impression.AJAX.analysefini = function(AJAXResp)
{
   logDebug('AJAX terminated (usefile)');
   if (AJAXResp.erreur) {
      Impression.AJAX.modifPrix(AJAXResp.erreur, false);
      Impression.popup.popupError(AJAXResp.erreur);
      Impression.settings.disableForm(true);
      logWarning('Erreur distante : ' + AJAXResp.erreur);
   } else {
      Impression.AJAX.modifPrix("Analyse terminée", true);
      Impression.settings.update();
      Impression.AJAX.modifNbPages(AJAXResp.nbPages);
   }
}

Impression.AJAX.recalcPrix = function()
{
   settings = Impression.settings;
   var url = "changeSettings?copies=" + settings.copies
     +"&couleur="+ settings.type_impression
     +"&papier="+ settings.papier
     +"&recto_verso="+ settings.disposition
     +"&agrafage="+ settings.agrafage
     +"&livret="+ settings.livret;
   this.modifPrix("Calcul en cours", true);
   this.call(url, this.changePrix);
}

Impression.AJAX.changePrix = function(AJAXResp)
{
   if (AJAXResp.erreur) {
      Impression.AJAX.modifPrix(AJAXResp.erreur, false);
      Impression.popup.popupError(AJAXResp.erreur);
      Impression.settings.disableForm(true);
      logWarning('Erreur distante : ' + AJAXResp.erreur);
   } else {
    Impression.AJAX.modifPrix("Coût : " + AJAXResp.nouvPrix + "€", false);
   }
}

Impression.AJAX.lancerImpression = function(AJAXResp)
{
   var url = "lancerImpression";
   Impression.settings.disableForm(true);
   this.call(url, this.finImpression);
}

Impression.AJAX.finImpression = function(AJAXResp)
{
   if (AJAXResp.erreur) {
      Impression.popup.popupError(AJAXResp.erreur);
      logWarning('Erreur distante : ' + AJAXResp.erreur);
   } else if (AJAXResp.SoldeInsuffisant) {
      Impression.popup.popupSolde();
      Impression.settings.disableForm(false);
      Impression.settings.update();
   } else {
      Impression.popup.popupImpression(AJAXResp.code, AJAXResp.code_bat_j);
   }
   Impression.AJAX.updateSolde();
}

Impression.AJAX.getPrinterState = function()
{
   var url = "etatImprimante";
   Impression.AJAX.call(url, Impression.AJAX.displayPrinterState);
}

Impression.AJAX.displayPrinterState = function(AJAXResp)
{
   if (AJAXResp.erreur) {
      logWarning('Erreur distante (etatImprimante) : ' + AJAXResp.erreur);
   } else  {
      show = document.createElement('ul')
      show.style.paddingLeft = '1em';
      show.style.marginLeft = '0';
      for( var i=0; i < AJAXResp.printer_state.length; i++) {
        var li = document.createElement('li');
        li.appendChild(document.createTextNode(AJAXResp.printer_state[i]));
        show.appendChild(li)
      }
      replaceChildNodes("etatImprimanteIci", show)
   }
}

Impression.AJAX.updateSolde = function()
{
   var url = "AJAXGetSolde";
   Impression.AJAX.call(url, Impression.AJAX.displaySolde);
}

Impression.AJAX.displaySolde = function(AJAXResp)
{
   if (AJAXResp.erreur) {
      logWarning('Erreur distante (etatImprimante) : ' + AJAXResp.erreur);
   } else  {
      replaceChildNodes("soldePlaceHolder", AJAXResp.solde )
   }
}

// on met a jour le status de l'imprimante toutes les 30 secondes
setInterval(Impression.AJAX.getPrinterState, 30000);
// on met a jours le solde de l'adherent toute les 5 minutes
setInterval(Impression.AJAX.updateSolde, 300000);
