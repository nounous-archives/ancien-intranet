#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Webirc"
    def icon(self):
       return "qwebircsmall.png"
    def category(self):
        return "Services"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('http://irc.crans.org/web/?nick=%s' % cherrypy.session['uid'])
    index.exposed = True

