#! /usr/bin/env python
# -*- coding: utf-8 -*-
# ##################################################################################################### #
#  Machines
# ##################################################################################################### #
#   Description:
#     Affiche la liste des machies, les infons sur une machine et permet des modifications
#   Informations:
#
#   Pages:
#     index:liste des machines +  le reste (AJAX)
#
# ##################################################################################################### #
import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time
# libraries crans
sys.path.append('/usr/scripts/gestion/')
sys.path.append('/usr/scripts/intranet/')
from intranet_utils import cp
if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
    from ldap_crans_test import *
#    print("mesmachines : unsing test ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])
else:
    from ldap_crans import *
#    print("mesmachines : unsing prod ldap : env=" + cherrypy.config.configMap["global"]["server.environment"])

from ClassesIntranet.ModuleBase import ModuleBase

class NotYourStuff(Exception):
    """Erreur levée si l'adhérent essaye de modifier une machine qu'il n'est pas une des siennes"""
    pass

class main(ModuleBase):
    _droits = ["Nounou", "Apprenti", "Cableur"]
    def title(self):
       return "Mes Machines"
    def icon(self):
       return "machines_icon_fixe.png"
    def category(self):
        return "Déprécié"

    _club = False

    def AJAXListeMachines(self):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        machines = []
        for une_machine in adh.machines():
            machineInfos = {}
            # nom, mac, mid, ip
            machineInfos['nom'] = une_machine.nom()
            machineInfos['nomCourt'] = une_machine.nom().split('.',1)[0]
            machineInfos['mid'] = une_machine.id()
            # type
            if une_machine.objectClass == 'machineFixe':
                machineInfos['type'] = 'fixe'
                #machineInfos['type'] = 'Machine fixe'
            else:
                machineInfos['type'] = 'wifi'
                #machineInfos['type'] = 'Machine wifi'
            # clef ipsec
            machines.append(machineInfos)

        return {"machines":machines}
    AJAXListeMachines.exposed = True

    def AJAXMachineInfo(self, mid):
        try:
            machine = cherrypy.session['LDAP'].search('mid=' + mid)['machine'][0]
            try:
                compte_machine = machine.proprietaire().compte()
            except:
                raise NotYourStuff("Il semble que vous cherchiez à modifier une machine dont le propriétaire n'a pas de compte Cr@ns…")
            if compte_machine != cherrypy.session['uid']:
                raise NotYourStuff("Vous ne pouvez pas modifier cette machine")
            # zamok -> pour tester l'affichage des ports, des alias
            #machine = cherrypy.session['LDAP'].search('mid=896')['machine'][0]
            machineInfos = {}
            # nom, mac, mid, ip
            machineInfos['nom'] = machine.nom()
            machineInfos['nomCourt'] = machine.nom().split('.',1)[0]
            machineInfos['mac'] = machine.mac()
            machineInfos['mid'] = machine.id()
            machineInfos['ip'] = machine.ip()
            machineInfos['ipv6'] = str(machine.ipv6())
            machineInfos['dnsIpv6'] = str(machine.dnsIpv6())
            # type
            if machine.objectClass == 'machineFixe':
                machineInfos['type'] = 'fixe'
            else:
                machineInfos['type'] = 'wifi'
            # clef ipsec
            try:
                machineInfos['ipsec'] = machine.ipsec()
            except:
                machineInfos['ipsec'] = ''
            # alias
            machineInfos['alias'] = machine.alias()
            # blacklists
            machineInfos['blacklist'] = []
            for blacklist_type in machine.blacklist_all()[0].keys():
               for (begin, end) in machine.blacklist_all()[0][blacklist_type]:
                  blacklist = {}
                  blacklist['begin'] = strftime('%d/%m/%Y %H:%M', localtime(int(begin)))
                  blacklist['end'] = strftime('%d/%m/%Y %H:%M', localtime(int(end)))
                  blacklist['type'] = blacklist_type
                  blacklist['actif'] = 1
                  machineInfos['blacklist'].append(blacklist)
            for blacklist_type in machine.blacklist_all()[1].keys():
               for (begin, end) in machine.blacklist_all()[1][blacklist_type]:
                  blacklist = {}
                  blacklist['begin'] = strftime('%d/%m/%Y %H:%M', localtime(int(begin)))
                  blacklist['end'] = strftime('%d/%m/%Y %H:%M', localtime(int(end)))
                  blacklist['type'] = blacklist_type
                  blacklist['actif'] = 0
                  machineInfos['blacklist'].append(blacklist)
            # ports
            machineInfos['ports'] = []
            if machine.portTCPin() != []:
                machineInfos['ports'].append(
                    {
                        'titre':'Ports TCP ouverts ext->machine',
                        'ports':machine.portTCPin()
                    }
                )
            if machine.portTCPout() != []:
                machineInfos['ports'].append(
                    {
                        'titre':'Ports TCP ouverts machine->ext',
                        'ports':machine.portTCPout()
                    }
                )
            if machine.portUDPin() != []:
                machineInfos['ports'].append(
                    {
                        'titre':'Ports UDP ouverts ext->machine',
                        'ports':machine.portUDPin()
                    }
                )
            if machine.portUDPout() != []:
                machineInfos['ports'].append(
                    {
                        'titre':'Ports TCP ouverts machine->ext',
                        'ports':machine.portUDPout()
                    }
                )

            return machineInfos
        except Exception, e:
            return {"erreur":str(e)}
    AJAXMachineInfo.exposed = True

    ##########################
    # affichage
    ##########################
    #
    # methode qui affiche la template
    #
    def index(self):
        return {
                'template'      :'machines',
                'values'        :{},
                'stylesheets'   :['machines.css'],
                'scripts':['machines.js'],
            }
    index.exposed = True


    ###########################################################################
    # methodes pour changer
    # des valeurs
    ###########################################################################
    #

    ##########################
    #   machine:nom
    ##########################
    def AJAXChangerNom(self, mid, nouveauNom):
        try:
            adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
            mach = cherrypy.session['LDAP'].search('mid=' + mid, 'w')['machine'][0]
            # tester si c'est bien la machine de l'adherent
            if mach.proprietaire().compte() != cherrypy.session['uid']:
                del adh, mach
                raise  Exception(u"L'uid de l'adherent ne correspond pas au proprietaire de la machine.")
            anciennom = mach.nom()
            nouveauNom = mach.nom(nouveauNom)
            mach.save()
            del mach
        except ValueError, e:
            raise e
            #return {'error':str(e)}
        cp.log("Changer nom machine : %s->%s" % (anciennom, nouveauNom), "MESMACHINES")
        return {'message':u"Modification réussie", 'mid':mid}
    AJAXChangerNom.exposed = True

    ##########################
    #   machine:mac
    ##########################
    def AJAXchangerMAC(self, mid, nouvelleMAC):
        try:
            adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
            mach = cherrypy.session['LDAP'].search('mid=' + mid, 'w')['machine'][0]
            # tester si c'est bien la machine de l'adherent
            if mach.proprietaire().compte() != cherrypy.session['uid']:
                del adh, mach
                raise  Exception(u"L'uid de l'adherent ne correspond mas au proprietaire de la machine.")
            anciennemac = mach.mac()
            nommachine = mach.nom()
            mach.mac(nouvelleMAC)
            mach.save()
            del mach
        except ValueError, e:
            return {'error':e.args[0]}
        cp.log("Change mac machine %s :  %s->%s" % (nommachine, anciennemac, nouvelleMAC), "MESMACHINES")
        return {'message':u"Modification réussie", 'mid':mid}
    AJAXchangerMAC.exposed = True



    ##########################
    #   machine:suppression
    ##########################
    def AJAXSupprimerMachine(self, mid):
        try:
            adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
            mach = cherrypy.session['LDAP'].search('mid=' + mid, 'w')['machine'][0]
            # tester si c'est bien la machine de l'adherent
            if mach.proprietaire().compte() != cherrypy.session['uid']:
                del adh, mach
                raise  Exception(u"L'uid de l'adherent ne correspond mas au proprietaire de la machine.")
            mach.delete()
        except ValueError, e:
            return {'error':e.args[0]}
        cp.log("Machine supprimee", "MACHINES")
        return {'message':u"Machine supprimée"}
    AJAXSupprimerMachine.exposed = True

    ##########################
    #   machine:creation
    ##########################
    def AJAXCreerMachine(self, nomNouvelleMachine, MACNouvelleMachine, typeNouvelleMachine):
        adh = cherrypy.session['LDAP'].getProprio(cherrypy.session['uid'])
        est_personnel = adh.etudes(0) == 'Personnel ENS'
        if est_personnel and typeNouvelleMachine == 'wifi':
            return {'error':'Vous n\'avez pas la possibilite d\'enregistrer de machine WiFi.'}
        if typeNouvelleMachine=='fixe' and (isinstance(adh,Adherent) and adh.droits() == []) and adh.machines_fixes() != [] and not est_personnel:
            return {'error':'Vous avez deja une machine fixe. Vous ne pouvez ajouter que des machines WiFi.'}
        try:
            if typeNouvelleMachine=='wifi':
                m = MachineWifi(adh)
            elif typeNouvelleMachine=='fixe':
                m = MachineFixe(adh)
            else:
            	raise Exception, "type de machine inconnu : %s " % typeNouvelleMachine
            m.nom(nomNouvelleMachine)
            m.ip("<automatique>")
            m.mac(MACNouvelleMachine)
            message = m.save()
            del m
        except ValueError, e:
            try:
                del m
            except:
                pass
            return {'error':e.args[0].replace("\n","\\n")}
        cp.log("Nouvelle machine %s" % nomNouvelleMachine, "MACHINES")
        return {'message':u"Machine enregistrée avec succès"}
    AJAXCreerMachine.exposed = True



