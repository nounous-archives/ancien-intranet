/* ************************************************************
 * Machines
 ************************************************************
 * Machines.init 		  	: initialisation de la page
 * Machines.listeMachines : liste des machines 
 * Machines.infoPane 		: information sur une machine 
 * Machines.actions 		: modifications, ajout...
 * Machines.AJAX     	  	: ajax 
 */
Machines = {};

/*****************************
    Machines.init
 *****************************/
Machines.init = function() 
{
   // afficher le tableau codelist
   appendChildNodes("globalDiv", Machines.createFrames());
   appendChildNodes("globalDiv", Machines.actions.makeMenu());
   // recuperer la liste des codes
   this.listeMachines.load();
}

Machines.createFrames = function() 
{
   var main  = DIV({"id":"gestion_machines_main_frame"}); 
   var inside  = DIV({"id":"__insideDivId", "style":"background:white;margin:5px;padding:5px;"},main); 
   var outside = DIV({"id":"__outsideDivId","style":"background:#000080;z-index:500;padding:0;"}, inside );
   roundElement(outside);
   roundElement(inside);
   log("Creation du cadre");
   return outside
}

/*****************************
    Machines.listeMachines
 *****************************/
Machines.listeMachines = {};


Machines.listeMachines.load = function() 
{
   Machines.AJAX.call("AJAXListeMachines", Machines.listeMachines.display);
   log("Chargement liste...");
   Machines.currentMachine = '';
}

Machines.listeMachines.display = function(result) 
{
   log("display liste");
   
   replaceChildNodes( "gestion_machines_main_frame",
		      H2({}, "Mes machines"), 
		      UL({"id":"listeMachines"}),
		      DIV( {"style":"clear:both;"} )
		      );
   if (result.machines) {
      replaceChildNodes('listeMachines',map(Machines.listeMachines.newMachineNodeFromDict ,result.machines));
      Machines.actions.updateMenu(Machines.actions.actionForMachineList);
      Machines.currentMid = '';
   }
   else if (result.erreur)
     logError("Erreur distante : " + result.erreur);
   else
     logError("Probleme bizarre...");
}

Machines.listeMachines.newMachineNodeFromDict = function (aDict, style) 
{
   var nom = aDict.nomCourt;
   var mid = aDict.mid;
   var image = "machines_icon_" + aDict.type + ".png";
   //log("create an item : " + nom);
   if (style) {
      var anIcon =  LI({'id':'machine_' + mid,"style":style});    	
   } else 
     var anIcon =  LI( {'id':'machine_' + mid} );
   appendChildNodes(anIcon, A({"href":"#", "onclick":"Machines.infoPane.loadInfo('"+mid+"');return false;"}, 
			      IMG({"src":"static/" + image}), 
			      SPAN({},nom) ) );
   return anIcon;
}

/*****************************
 Machines.infoPane
 *****************************/
Machines.infoPane = {};

Machines.infoPane.loadInfo = function(mid) 
{
   if (!mid)
     if (!Machines.currentMachine) {
	logError("Machines.infoPane.loadInfo : pas de mid, pas de machine courrante...");
	return
     } else
     mid = Machines.currentMachine.mid;
   
   log("load info : " + mid);
   Machines.AJAX.call("AJAXMachineInfo?mid=" + mid, Machines.infoPane.display);
   try {
      pulsate('machine_' + mid);
   } catch (error) {}
}

Machines.infoPane.display = function(result) 
{
   if (result.nom) {
      log("displaying info : " + result.mid);
      // building pane
      var machinePane = DIV({"id":"machine_pane"});
      
      // lien pour retourner a la liste des machines
      var back_link = A({"href":"#", "onclick":"Machines.listeMachines.load(); return false;"},"Retour");
      appendChildNodes( machinePane, back_link );
      
      // titre (nom de machine + alias)
      var title = H2({}, IMG({"src":"./static/machines_icon_" + result.type + ".png"}), " ", result.nom);
      var alias = DIV({"class":"alias"}, 
		      map(function(a_name) { return SPAN({}, a_name + " ")}, result.alias));
      appendChildNodes( title, alias );
      appendChildNodes( machinePane, title );
      
      // infos de base (type, mac, ip, ipsec...)
      var basicInfos = createDOM("DL", {"class":"basicInfos"});
      appendChildNodes(basicInfos, createDOM("DT", "type:" ), createDOM("DD",{},"Machine " + result.type ) );
      appendChildNodes(basicInfos, createDOM("DT", "mac: " ), createDOM("DD",{},result.mac ) );
      appendChildNodes(basicInfos, createDOM("DT", "ip: " ), createDOM("DD",{},result.ip ) );
      appendChildNodes(basicInfos, createDOM("DT", "ipv6: " ), createDOM("DD",{},result.ipv6 ) );
      appendChildNodes(basicInfos, createDOM("DT", "accessible directement en ipv6: " ), createDOM("DD",{},result.dnsIpv6 ) );
      if (result.ipsec) {
	    appendChildNodes(basicInfos, createDOM("DT", "Clef Wifi:" ), createDOM("DD",{},result.ipsec, BR(), A({'href': 'http://wifi.crans.org/#Config'}, 'Comment configurer ma machine')));
        // http://wifi.crans.org/#Config
      }
      appendChildNodes( machinePane, basicInfos );
      appendChildNodes( machinePane, DIV( {"style":"clear:both;"} ) );
      

      // blacklist
      if (result.blacklist)
        if (result.blacklist.length) {
	   var subtitle = H3({}, "Blacklist");
	   appendChildNodes( machinePane, subtitle );
	   var blacklist_table = TABLE({"class":"blacklist_table", "cellspacing":"0"},
				       THEAD({},
					     TH({}, "Cause"), 
					     TH({}, "Debut"),
					     TH({}, "Fin")
					     )
				       );
	   map(function(a_blacklist_dict) {
	      var style = "inactif";
	      if (a_blacklist_dict.actif)
		style = "actif";
	      var line = TR({"class":style},
			    TD({}, a_blacklist_dict.type),
			    TD({}, a_blacklist_dict.begin),
			    TD({}, a_blacklist_dict.end)
			    );
	      appendChildNodes(blacklist_table, line); 
	   }, result.blacklist );
	   appendChildNodes( machinePane, blacklist_table );
	}
      // liste des ports ouverts
      if (result.ports) 
	if (result.ports.length) {
	   var subtitle = H3({}, "Ports ouverts");
	   appendChildNodes( machinePane, subtitle );
	   var port_table = TABLE({"class":"ports_table", "cellspacing":"0"});
	   map(function(a_port_dict) {
	      var line = TR({},
			    TH({}, a_port_dict.titre),
			    map(function(a_port) {return TD({}, a_port);}, a_port_dict.ports)
			    );
	      appendChildNodes(port_table, line); 
	   }, result.ports );
	   appendChildNodes( machinePane, port_table );
	}
      
      // attaching pane to document
      replaceChildNodes( "gestion_machines_main_frame", machinePane);
      // updationg actions
      Machines.currentMachine = result;
      Machines.actions.updateMenu(Machines.actions.actionsForInfoPane);
   } else if (result.erreur) {
      logError("Erreur distante : " + result.erreur);
   } else
     logError("Probleme bizarr...");
   
}

/*****************************
 Machines.actions
 *****************************/
Machines.actions = {}

Machines.actions.disponibles = 
{
     'Ajouter machine fixe':'Machines.actions.formulaire.nouvelleMachine(\'fixe\');',
     'Ajouter machine wifi':'Machines.actions.formulaire.nouvelleMachine(\'wifi\');',
     'Modifier Mac':'Machines.actions.formulaire.modifierMac(Machines.currentMid);',
     'Renommer':'Machines.actions.formulaire.renommer(Machines.currentMid);',
     'Supprimer':'Machines.actions.formulaire.supprimer(Machines.currentMid);',
     'Demander l\'ouverture d\'un port':'Machines.actions.formulaire.ouverturePort(Machines.currentMid);'
}
Machines.actions.actionForMachineList = ['Ajouter machine fixe', 'Ajouter machine wifi'];
Machines.actions.actionsForInfoPane = ['Modifier Mac', 'Renommer', 'Supprimer'];

Machines.actions.makeMenu = function(actionListe)
{
   log("building action menu");
   var liste = UL({"id":"liste_actions"});
   //this.updateMenu(actionListe, liste)
   return DIV( {"id":"menu_actions"},
	       H1( {}, "ACTIONS"),
	       liste
	       );
}

Machines.actions.updateMenu = function(actionListe, liste_actionsNode)
{
   if (!liste_actionsNode)
     liste_actionsNode = "liste_actions";
   replaceChildNodes(liste_actionsNode);
   map(
       function(actionName) {
	  appendChildNodes(liste_actionsNode, LI({}, Machines.actions.makeActionLink(actionName)));
       }
       , actionListe);
}

Machines.actions.makeActionLink = function(actionName)
{
   if (this.disponibles[actionName])
     {
	return A({"href":"#", "onclick":this.disponibles[actionName] + "return false;"}, actionName);
     } else
     logError("action inconnue " + actionName);
   return A({}, "???");
}



Machines.actions.callback = function(result) {
   if (result.message){
      log(result.message);
      Crans.messages.setMessage(result.message);
      if (result.mid)
	Machines.infoPane.loadInfo(result.mid);
      else
	Machines.listeMachines.load();
   }
   if (result.error){
      alert(result.error);
      logError("AJAX error");
   }	
}

// actions : affichage des formulaires
Machines.actions.formulaire = {}
Machines.actions.formulaire.modifierMac = function() 
{
   if (!Machines.currentMachine)
     logError("pas de machine courrante");
   else {
      var c = '';
      while ( c == '') 
	c = prompt("Adresse MAC de la machine :",Machines.currentMachine.mac);
      if (c == null)
	return false;
      // AJAX stuff
      Machines.AJAX.call('AJAXchangerMAC?nouvelleMAC=' + c + '&mid=' + Machines.currentMachine.mid, 
			 Machines.actions.callback);
   }
   return false;
}
Machines.actions.formulaire.renommer = function() {
   if (!Machines.currentMachine)
     logError("pas de machine courrante");
   else {
      var c = '';
      while ( c == '') 
	c = prompt("Nom de la machine :",Machines.currentMachine.nomCourt);
      if (c == null)
	return false;
      // AJAX stuff
      Machines.AJAX.call('AJAXChangerNom?nouveauNom=' + c + '&mid=' + Machines.currentMachine.mid, 
			 Machines.actions.callback);
   }
   return false;
}
Machines.actions.formulaire.supprimer = function() {
   if (!Machines.currentMachine)
     logError("pas de machine courrante");
   else {
      if (confirm("Supprimer la machine " + Machines.currentMachine.nomCourt +" ?")) {
	 // AJAX stuff
	 Machines.AJAX.call('AJAXSupprimerMachine?mid=' + Machines.currentMachine.mid, 
			    Machines.actions.callback);
      }
   }
   return false;
}

Machines.actions.formulaire.nouvelleMachine = function(type) {
   replaceChildNodes( "gestion_machines_main_frame",
		      A({"href":"#", "onclick":"Machines.listeMachines.load(); return false;"},"Retour"),
		      H2({}, "Nouvelle machine"), 
		      FORM({"style":"clear:both;", 'onsubmit':'Machines.actions.creerMachine(this.typeField.value, this.nom.value, this.mac.value);return false;'}, 
			   createDOM('label', {'for':'add_machine_nom'}, "Nom de la machine : "),
			   INPUT({"name":"nom"}),
			   BR(),
			   createDOM('label', {'for':'add_machine_mac'}, "Adresse MAC : "),
			   INPUT({"name":"mac"}),
			   BR(),
			   createDOM('label', {'for':'add_machine_type'}, "Type de machine : "),
			   SPAN({'id':'add_machine_type'}, type),
			   INPUT({"name":"typeField", 'type':'hidden', 'value':type}),
			   BR(),
			   BUTTON({"class":"liens", 'type':'submit'}, 'Ajouter')
			   )
		      );
   return false;
}

Machines.actions.creerMachine = function(type, nom, mac) {
   Machines.AJAX.call('AJAXCreerMachine?nomNouvelleMachine=' + nom
		      + '&MACNouvelleMachine=' + mac
		      + '&typeNouvelleMachine=' + type,
		      Machines.actions.callback);		
}

/*****************************
 Machines.AJAX
 *****************************/
Machines.AJAX = {}

Machines.AJAX.call = AJAX.call
