from ClassesIntranet.ModuleBase import ModuleBase


class main(ModuleBase):
    def category(self):
        return u"Services"
    def title(self):
       return u"Informations utiles"

    def index(self):
        return {
          'template':'info-diverses',
          'values':{},
          'stylesheets':['infos.css'],
        }
    index.exposed = True
