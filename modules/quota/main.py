#! /usr/bin/env python
import cherrypy
import tempfile
import shutil
import os
import sys

sys.path.append('/usr/scripts/intranet/')
import intranet_utils.cp as cp
from ClassesIntranet.ModuleBase import ModuleBase
sys.path.append('/usr/scripts/')
import utils.quota as quota

class main(ModuleBase):
    _droits=["personnel"]
    def category(self):
        return "Personnel"
    def title(self):
       return "Quotas"
    def icon(self):
        try:
            quotas = self._get_quota()
            for a_quota in quotas:
                if a_quota['quota'] < a_quota['usage']:
                    return "icon_alert.png"
            return "icon.png"
        except:
            return "icon_disabled.png"

    def _get_quota(self):
        if (cherrypy.config.configMap["global"]["server.environment"] == "development"):
            return quota.fake_getUserQuota(cherrypy.session['uid'])
        else:
            return quota.getUserQuota(cherrypy.session['uid'])


    ##########################
    # affichage
    ##########################
    #
    # methode qui affiche la template
    #
    def index(self ):
        values = {}
        try:
            quotas = self._get_quota()
            returned_quotas = []
            for a_quota in quotas:
               # calculate text equivalent
               quota = a_quota['quota']
               usage = a_quota['usage']
               limite = a_quota['limite']
               text_equiv = "["
               text_equiv+= "#" * min( 10, int( 10 * usage / quota ) )
               text_equiv+= "." * max(0, int( 10 * ( quota - usage ) / quota ) )
               if limite > quota:
                   text_equiv+= "|"
                   limite_diff = limite - quota
                   diff_proportion = 10 * limite_diff / quota
                   depassement = max(0, usage - quota)
                   text_equiv+= "#" * min(0, int(diff_proportion* ( depassement / limite_diff ) ) )
                   text_equiv+= "." * max(0, int(diff_proportion*( limite_diff - depassement ) / limite_diff ) )
               text_equiv+= "]"
               a_returned_quota = {
                        "label":a_quota['label'],
                        "usage":a_quota['usage'],
                        "quota":a_quota['quota'],
                        "limite":a_quota['limite'],
                        "percents":a_quota['%'],
                        "%":a_quota['%'],
                        "text_equiv":text_equiv,
                        "svg_url":"barreSVG?filesystem=%s" % a_quota['filesystem'],
               }
               returned_quotas.append(a_returned_quota)
            values =  {'quotas': returned_quotas}
        except Exception, e:
            cp.log('error getting quota for user %s : %s' % (cherrypy.session['uid'], str(e)), 'QUOTA', 1)
            values =  {'erreur':str(e)}
        return {'template':'quota',
                'values': values,
                'stylesheets':['quota.css'],
                'scripts':['quota.js', 'popup.js'],
            }
    index.exposed = True

    def index_html(self ):
        result = self.index()
        result['template'] = 'quota_html'
        return result
    index_html.exposed = True


    ##########################
    # SVG
    ##########################
    #
    # methode qui renvoie une barre en svg
    #
    def barreSVG(self, filesystem = ""):
        try:
            values = {'erreur':"Not found"}
            quotas = self._get_quota()
            for a_quota in quotas:
                if a_quota['filesystem'] == filesystem:
                    values = {
                        "usage":a_quota['usage'],
                        "quota":a_quota['quota'],
                        "limite":a_quota['limite'],
                        "percents":a_quota['%'],
                    }
        except Exception, e:
            values =  {'erreur':str(e) }
        cherrypy.response.headers['Content-Type']="image/svg+xml"
        return {'template':'barre.svg',
                'values': values,
                'standalone':True,
            }
    barreSVG.exposed= True
