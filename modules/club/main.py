from ClassesIntranet.ModuleBase import ModuleBase


class main(ModuleBase):
    _droits = [ "Imprimeur_club", "Responsable_club", "Nounou", "Bureau" ]
    def category(self):
        return u"Imprimeur"
    def title(self):
       return u"Connexion en tant que Club"


    def verifLogin(self,login):
        LDAP = cherrypy.session['LDAP']
        results = LDAP.search("login=%s" % login)
        clubs = results['club']
        adh = results['adherent']
        if adh and adh[0].id() == cherrypy.session['aid']:
            cherrypy.session['uid'] = login
            cherrypy.session['droits'] = adh[0].droits()
            if adh[0].etudes(0) == 'Personnel ENS':
                cherrypy.session['droits'] = ["personnel"]
            cherrypy.session['estClub'] = False
            cherrypy.session['session_key'] = True
            cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
            cherrypy.session['username'] = login
        elif clubs and (cherrypy.session['aid'] in clubs[0].imprimeurs() or cherrypy.session['aid'] == clubs[0].responsable().id()):
            adh = LDAP.search('aid=%s' % cherrypy.session['aid'])['adherent'][0]
            cherrypy.session['uid'] = login
            cherrypy.session['droits'] = []
            cherrypy.session['estClub'] = True
            cherrypy.session['adh_uid'] = adh.compte()
            cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
            cherrypy.session['username'] = login
        elif clubs and clubs[0].compte() == 'club-crans' and (u'Nounou' in cherrypy.session['droits'] or u'Bureau' in cherrypy.session['droits']):
            adh = LDAP.search('aid=%s' % cherrypy.session['aid'])['adherent'][0]
            cherrypy.session['uid'] = login
            cherrypy.session['droits'] = []
            cherrypy.session['estClub'] = True
            cherrypy.session['adh_uid'] = adh.compte()
            cherrypy.log("User logged in : %s" % cherrypy.session['uid'], "LOGIN")
            cherrypy.session['username'] = login

        raise cherrypy.HTTPRedirect('https://intranet.crans.org')

    def index(self, login_club=None):
        if login_club:
            self.verifLogin(login_club)
        LDAP = cherrypy.session['LDAP']
        clubs = LDAP.search("imprimeurClub=%s" % cherrypy.session['aid'] )['club']
        clubs.extend(LDAP.search("responsable=%s" % cherrypy.session['aid'] )['club'])
        club_list = []
        for club in clubs:
            if club.compte():
                club_list.append({
                  'name' : club.nom(),
                  'login' : club.compte(),
                  'url' : '?login_club=%s' % club.compte(),
                  'icon' : 'static/icon.png'
                })
        if u'Nounou' in cherrypy.session['droits'] or u'Bureau' in cherrypy.session['droits']:
            club_list.append({
              'name' : 'Cr@ns',
              'login' : 'club-crans',
              'url' : '?login_club=club-crans',
              'icon' : 'static/icon.png'
            })
        return {
          'template':'club',
          'values':{'club_list' : club_list},
          'stylesheets':['club.css'],
        }
    index.exposed = True
