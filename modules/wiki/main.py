#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Compte Wiki"
    def icon(self):
       return "wiki_icon.png"
    def category(self):
        return "Personnel"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://intranet2.crans.org/wiki/')
    index.exposed = True

