#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Téléphonie VoIP"
    def icon(self):
       return "icone_voip.png"
    def category(self):
        return "Beta"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://intranet2.crans.org/voip/')
    index.exposed = True

