#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-
# ##################################################################################################### #
#  Factures (gestion)
# ##################################################################################################### #
#   Description:
#     Permet de chercher dans les facture, d'en créditer et d'en supprimer
#   Informations:
#
#   Pages:
#     index:afiche le formulaire et les factures
#
# ##################################################################################################### #
import cherrypy
import sys
import os
import datetime
sys.path.append('/usr/scripts/intranet/')
from intranet_utils import cp
import cranslib.utils.exceptions
from ClassesIntranet.ModuleBase import ModuleBase

class main(ModuleBase):
    _droits = ["Imprimeur"]
    def title(self):
       return "Gestion factures"
    def category(self):
       return "Imprimeur"

    def index(self, message = '', erreur = ''):
        if cherrypy.session.has_key('gestion_factures-current_search'):
            del cherrypy.session['gestion_factures-current_search']
        return self.displayTemplate()
    index.exposed = True

    def search(self, fid=None, uid=None, aid=None):
        cherrypy.session['gestion_factures-current_search'] = {
                "fid":fid,
                "uid":uid,
                "aid":aid,
            }
        return self.displayTemplate()
    search.exposed = True



    def displayTemplate(self, message = '', erreur = ''):
        t = {}
        t['message'] = message
        t['error'] = erreur
        if cherrypy.session.has_key('gestion_factures-current_search'):
            fid = cherrypy.session['gestion_factures-current_search']['fid']
            uid = cherrypy.session['gestion_factures-current_search']['uid']
            aid = cherrypy.session['gestion_factures-current_search']['aid']
            t['listeFactures'] = self.buildInvoiceList(
                                    fid = fid,
                                    uid = uid,
                                    aid = aid,
                                 )

        else:
            fid = ""
            uid = ""
            aid = ""
        t["form"] = []
        t["form"]+= [{'name':'fid', 'label':'fid', 'value':fid}]
        t["form"]+= [{'name':'uid', 'label':'login', 'value':uid}]
        t["form"]+= [{'name':'aid', 'label':'aid', 'value':aid}]

        return {
            'template'      :'factures-gestion',
            'values'        :t,
            'stylesheets'   :['cransFactures.css'],
        }




    def buildInvoiceList(self, fid=None, uid=None, aid=None):
        ##############  liste des factures  ##############
        if fid:
            search_string = "fid=%s" % str(fid)
            liste_factures_ldap = cherrypy.session['LDAP'].search(search_string)['facture']
        elif uid:
            search_string = "uid=%s" % str(uid)
            try:
                liste_factures_ldap = cherrypy.session['LDAP'].search(search_string)['adherent'][0].factures()
            except:
               liste_factures_ldap = []
        elif aid:
            search_string = "aid=%s" % str(aid)
            try:
                liste_factures_ldap = cherrypy.session['LDAP'].search(search_string)['adherent'][0].factures()
            except:
               liste_factures_ldap = []
        else:
            return []
        liste_factures_affichees = []
        for f in liste_factures_ldap:
            try:
                facture = {}
                facture['no'] = f.numero()
                proprio = f.proprietaire()
                if proprio.objectClass == 'club':
                    proprio = proprio.responsable()
                facture['adherent'] = proprio.mail()
                facture['montant'] = f.total()
                facture['payee'] = f.recuPaiement()
                facture['date'] = f.historique()[0].split(',')[0]
                facture['url'] = f.urlPaypal()
                facture['intitule'] = f.articles()[0]['designation']
                facture['details'] = [
                    {
                    'intitule':art['designation'],
                    'quantite':art['nombre'],
                    'prixUnitaire':art['pu'],
                    'prixTotal':art['pu']*art['nombre'],
                    } for art in f.articles()]
                liste_factures_affichees.append(facture)
            except:
                cp.log("Facture non affichable : fid=%s" % str(f.numero()), "GESTION FACTURES", 1)

        liste_factures_affichees.sort(lambda x,y : cmp(int (y['no']), int(x['no'])))
        return liste_factures_affichees

    def delFacture(self, fid):
        try:
            # trrouver la factures
            fact = cherrypy.session['LDAP'].search('fid=' + fid, 'w')['facture'][0]
            # la supprimer
            fact.delete()
        except Exception, e:
            cp.log(unicode(e), "GESTION FACTURES", 1)
            return self.index(erreur=u"Probleme lors de la suppression")
        cp.log(u"Facture supprimee [fid=%s]" % fid, "GESTION FACTURES")
        return self.displayTemplate(message=u"Facture suprimée")
    delFacture.exposed = True

    def crediteFacture(self, fid):
        try:
            # trrouver la factures
            fact = cherrypy.session['LDAP'].search('fid=' + fid, 'w')['facture'][0]
            # la supprimer
            fact.recuPaiement(cherrypy.session['uid'])
            fact.save()
        except Exception, e:
            cp.log(unicode(e), "GESTION FACTURES", 1)
            cp.log(cranslib.utils.exceptions.formatExc(), "GESTION FACTURES", 1)
            return self.displayTemplate(erreur=u"Erreur: " + unicode(e) )
        cp.log("Facture creditee [fid=%s]" % fid, "GESTION FACTURES")
        return self.displayTemplate(message=u"Facture créditée")
    crediteFacture.exposed = True
