#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import sys
import os
import datetime
from time import strftime, localtime, time

class main(ModuleBase):
    _droits = ["personnel"]
    def title(self):
       return "Webnews"
    def icon(self):
       return "news_icon.png"
    def category(self):
        return "Services"

    _club = False

    def index(self):
        raise cherrypy.HTTPRedirect('https://news.crans.org/newsgroups.php?cas=crans.org')
    index.exposed = True

